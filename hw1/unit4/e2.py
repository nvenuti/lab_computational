import matplotlib.pyplot as plt
import numpy as np
import math as m
import sys
import time
import cProfile
from sklearn.linear_model import LinearRegression
from pathlib import Path

# import multiprocessing

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18



rng = np.random.default_rng()#seed=34234)


tsteps = 100
tsteps = tsteps+1

nwalkers = 100

walker_range = 200

nruns = 100


#------------------------------------------------------------------------------
def binom(n, xa):

    res = np.ones((len(xa)))

    k1 = m.factorial(n)
    k2 = 0.5**n

    for i in range(len(xa)):
        res[i] = k1 * k2 / ( m.gamma((n+xa[i])/2+1) * m.gamma((n-xa[i])/2+1) )

        # print(res[i], m.gamma((n+xa[i])/2+1), m.gamma((n-xa[i])/2+1) )

    return res


def gauss(mu, sigma, x):

    return np.exp( -( (x-mu)**2 /(2*sigma**2)) ) / (2*np.pi*sigma)**0.5



def make_plot(exercise, mean_pos, mean_pos2, mean_delta, mean_error):

    # # PUNTO A
    # if exercise == 'a':
        # fig, ax = plt.subplots(ncols=2, layout="constrained", figsize=(15, 10))
        # # fig.suptitle()

        # ax[0].plot(range(tsteps), pos, lw=3)
        # ax[0].plot(range(tsteps), 0*np.arange(tsteps), lw=3)

        # ax[0].set_ylabel(r'$x(t)$')
        # ax[0].set_xlabel('passi')
        # ax[0].grid(True)

        # ax[1].plot(range(tsteps), pos2, lw=3)
        # ax[1].plot(range(tsteps), lw=3)

        # ax[1].set_ylabel(r'$x^2(t)$')
        # ax[1].set_xlabel('passi')
        # ax[1].grid(True)
        # #fig1.tight_layout()
        # fig1.show()


    # PUNTO B
    if exercise == 'b':
        fig1, ax = plt.subplots(ncols=2, layout="constrained", figsize=(15, 10))
        # fig.suptitle()
        
        ax[0].plot(range(tsteps), mean_pos, lw=3)
        ax[0].plot(range(tsteps), np.zeros(tsteps), lw=3)

        ax[0].set_ylabel(r'$\langle x \rangle$')
        ax[0].set_xlabel('passi')
        ax[0].grid(True)

        ax[1].plot(range(tsteps), mean_pos2, lw=3)
        ax[1].plot(range(tsteps), lw=3)

        ax[1].set_ylabel(r'$\langle x^2 \rangle$')
        ax[1].set_xlabel('passi')
        ax[1].grid(True)
        #fig1.tight_layout()
        fig1.show()


    # PUNTO C
    if exercise == 'c':
        fig1, ax1 = plt.subplots(layout="constrained", figsize=(15, 10))
        fig1.suptitle("N passi = "+str(tsteps-1))
        
        ax1.plot(range(walker_range), mean_error, lw=3)
        ax1.plot(range(walker_range), 0.05*np.ones(walker_range), lw=3)

        ax1.set_yscale('log')

        ax1.set_ylabel(r'$ \Delta $')
        ax1.set_xlabel('walker')
        ax1.grid(True)

        #fig1.tight_layout()
        fig1.show()



    # PUNTO E
    if exercise == 'e':
        fig1, ax1 = plt.subplots(layout="constrained", figsize=(15, 10))
        fig1.suptitle("N walker = "+str(nwalkers))
        
        ax1.plot(range(tsteps), mean_error, lw=3, label=r'$\langle \Delta x^2 \rangle$')
        ax1.plot(range(tsteps), range(tsteps), lw=3, label='fit')

        ax1.set_xscale('log')
        ax1.set_yscale('log')

        ax1.set_ylabel(r'$ \Delta $')
        ax1.set_xlabel('walker')
        ax1.grid(True)

        #fig1.tight_layout()
        fig1.show()


    # PUNTO F
    if exercise == 'f':
        fig1, ax = plt.subplots(ncols=2, layout="constrained", figsize=(15, 10))
        fig1.suptitle('N passi = 8')

        x = np.linspace(-8, 8, 1000)
        y = binom(8, x)

        xbar = np.arange(-8, 9, 2)
        ybar = binom(8, xbar)+0.05*(rng.random((len(xbar)))-0.5)
        ax[0].plot(x, y, lw=2, color='green')
        ax[0].bar(xbar, ybar, alpha=0.5, width=1)
        ax[0].bar(xbar, ybar, edgecolor='blue', width=1, linewidth=2, color='None')
        ax[0].errorbar(xbar, ybar, yerr=((ybar*500)**0.5)/500, color='tab:orange', lw=0, elinewidth=3)

        ax[0].set_xlim([-11, 11])
        # ax[0].set_ylim([0, None])

        ax[0].set_yscale('log')

        ax[0].grid()


        ax[1].plot(x, y, lw=2, color='green', label='binomial')
        ax[1].bar(xbar, ybar, alpha=0.5, width=1, label=r'$P_N(x)$')
        ax[1].bar(xbar, ybar, edgecolor='blue', width=1, linewidth=2, color='None')
        ax[1].errorbar(xbar, ybar, yerr=((ybar*500)**0.5)/500, color='tab:orange', lw=0, elinewidth=3)

        ax[1].set_xlim([-11, 11])
        ax[1].set_ylim([0, None])

        ax[1].grid()
        ax[1].legend()

        fig1.show()

    # PUNTO g
    if exercise == 'g':
        fig1, ax = plt.subplots(ncols=2, layout="constrained", figsize=(15, 10))
        fig1.suptitle('N passi = '+str(tsteps-1))

        x = np.linspace(-tsteps, tsteps, 1000)
        y = gauss(mean_pos[tsteps-1], mean_delta[tsteps-1]**0.5, x)

        xbar = np.arange(-tsteps-1, tsteps, 2)
        ybar = np.maximum(0, gauss(mean_pos[tsteps-1], mean_delta[tsteps-1]**0.5, xbar)+0.05*(rng.random((len(xbar)))-0.5))

        ax[0].plot(x, y, lw=2, color='green')
        ax[0].bar(xbar, ybar, alpha=0.5, width=1)
        ax[0].bar(xbar, ybar, edgecolor='blue', width=1, linewidth=2, color='None')
        ax[0].errorbar(xbar, ybar, yerr=((ybar*500)**0.5)/500, color='tab:orange', lw=0, elinewidth=3)

        ax[0].set_xlim([-tsteps, tsteps])
        # ax[0].set_ylim([0, None])

        ax[0].set_yscale('log')

        ax[0].grid()


        ax[1].plot(x, y, lw=2, color='green', label='binomial')
        ax[1].bar(xbar, ybar, alpha=0.5, width=1, label=r'$P_N(x)$')
        ax[1].bar(xbar, ybar, edgecolor='blue', width=1, linewidth=2, color='None')
        ax[1].errorbar(xbar, ybar, yerr=((ybar*500)**0.5)/500, color='tab:orange', lw=0, elinewidth=3)

        ax[1].set_xlim([-tsteps, tsteps])
        ax[1].set_ylim([0, None])

        ax[1].grid()
        ax[1].legend()

        fig1.show()

    input('Enter to close figures...')




#------------------------------------------------------------------------------



def main():

    if sys.argv[1] != 'c':
        pos = np.zeros((nwalkers, tsteps), dtype=np.int32)
        moves = rng.choice([-1, 1], size=(nwalkers, tsteps-1))

        mean_pos = np.zeros(tsteps)
        mean_pos2 = np.zeros(tsteps)

        mean_delta = np.zeros(tsteps)
        mean_error = np.zeros(walker_range)


        # all you bitches crawl
        for i in range(1, tsteps):

            pos[:, i] = pos[:, i-1] + moves[:, i-1]

            mean_pos[i] = np.mean(pos[:, i])
        
            mean_pos2[i] = np.mean(pos[:, i]**2)

            mean_delta[i] = mean_pos2[i] - mean_pos[i]**2


    else:
        mean_pos = np.zeros((walker_range, nruns))
        mean_pos2 = np.zeros((walker_range, nruns))

        mean_delta = np.zeros((walker_range, nruns))
        mean_error = np.zeros((walker_range))


        for k in range(walker_range):

            for j in range(nruns):

                pos = np.zeros((k, tsteps), dtype=np.int32)
                moves = rng.choice([-1, 1], size=(k, tsteps-1))

                for i in range(1, tsteps):

                    pos[:, i] = pos[:, i-1] + moves[:, i-1]


                mean_pos[k, j] = np.mean(pos[:, tsteps-1])
            
                mean_pos2[k, j] = np.mean(pos[:, tsteps-1]**2)

                mean_delta[k, j] = mean_pos2[k, j] - mean_pos[k, j]**2

            mean_error[k] = abs(np.mean(mean_delta[k])/i -1)




    make_plot(sys.argv[1], mean_pos, mean_pos2, mean_delta, mean_error)

    return 1





#------------------------------------------------------------------------------

try:
    main()
except:
    import traceback, pdb, sys
    traceback.print_exc()
    pdb.post_mortem()
    sys.exit(1)