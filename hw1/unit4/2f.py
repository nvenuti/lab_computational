import matplotlib.pyplot as plt
import numpy as np
import math as m
# import scipy


plt.rcParams["mathtext.fontset"] = "cm"
# plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18


rng = np.random.default_rng() #seed=34234)


def binom(n, xa):

    res = np.ones((len(xa)))

    k1 = m.factorial(n)
    k2 = 0.5**n

    for i in range(len(xa)):
        res[i] = k1 * k2 / ( m.gamma((n+xa[i])/2+1) * m.gamma((n-xa[i])/2+1) )

        # print(res[i], m.gamma((n+xa[i])/2+1), m.gamma((n-xa[i])/2+1) )

    return res


fig, ax = plt.subplots(ncols=2, layout="constrained", figsize=(15, 10))
fig.suptitle('N passi = 8')

x = np.linspace(-8, 8, 1000)
y = binom(8, x)

xbar = np.arange(-7, 8, 2)
ybar = binom(8, xbar)+0.05*(rng.random((len(xbar)))-0.5)
ax[0].plot(x, y, lw=2, color='green')
ax[0].bar(xbar, ybar, alpha=0.5, width=1)
ax[0].bar(xbar, ybar, edgecolor='blue', width=1, linewidth=2, color='None')
ax[0].errorbar(xbar, ybar, yerr=((ybar*500)**0.5)/500, color='tab:orange', lw=0, elinewidth=3)

ax[0].set_xlim([-11, 11])
# ax[0].set_ylim([0, None])

ax[0].set_yscale('log')

ax[0].grid()


ax[1].plot(x, y, lw=2, color='green', label='binomial')
ax[1].bar(xbar, ybar, alpha=0.5, width=1, label=r'$P_N(x)$')
ax[1].bar(xbar, ybar, edgecolor='blue', width=1, linewidth=2, color='None')
ax[1].errorbar(xbar, ybar, yerr=((ybar*500)**0.5)/500, color='tab:orange', lw=0, elinewidth=3)

ax[1].set_xlim([-11, 11])
ax[1].set_ylim([0, None])

ax[1].grid()
ax[1].legend()

plt.savefig('2f.png')
