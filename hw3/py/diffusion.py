import matplotlib.pyplot as plt
import numpy as np
import math as m
import sys
import cProfile
from sklearn.linear_model import LinearRegression


plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18

#from numba import njit


rng = np.random.default_rng()

#------------------------------------------------------------------------------
#initialize grid with random occupation
def make_initial_grid(gridsize, nparticles):

    # picture of the grid
    grid = np.zeros((gridsize, gridsize), dtype=bool)
    
    # n sites occupied
    sites = rng.choice(gridsize**2, size=nparticles, replace=False)

    # make the grid picture
    x_pos = sites % gridsize
    y_pos = sites // gridsize

    grid[x_pos, y_pos] = True

    return grid, x_pos, y_pos

# either loop over the particles or make n random choices,
# then attempt move in random direction
def iterator(gridsize, nparticles, grid, x_pos, y_pos, x_diff, y_diff, random_evo):

    # let's do it the dumb way
    directions = rng.random(nparticles)

    fail = 0

    if not random_evo:
        for i in range(nparticles):

            if directions[i] < 0.25:
                if grid[(x_pos[i]+1) % gridsize, y_pos[i]] == False:
                    grid[x_pos[i], y_pos[i]] = False
                    grid[(x_pos[i]+1) % gridsize, y_pos[i]] = True
                    x_pos[i] = (x_pos[i]+1) % gridsize
                    x_diff[i] += 1
                else:
                    fail += 1

            elif directions[i] < 0.5:
                if grid[x_pos[i], (y_pos[i]-1) % gridsize] == False:
                    grid[x_pos[i], y_pos[i]] = False
                    grid[x_pos[i], (y_pos[i]-1) % gridsize] = True
                    y_pos[i] = (y_pos[i]-1) % gridsize
                    y_diff[i] -= 1
                else:
                    fail += 1

            elif directions[i] < 0.75:
                if grid[(x_pos[i]-1) % gridsize, y_pos[i]] == False:
                    grid[x_pos[i], y_pos[i]] = False
                    grid[(x_pos[i]-1) % gridsize, y_pos[i]] = True
                    x_pos[i] = (x_pos[i]-1) % gridsize
                    x_diff[i] -= 1
                else:
                    fail += 1

            elif directions[i] > 0.75:
                if grid[x_pos[i], (y_pos[i]+1) % gridsize] == False:
                    grid[x_pos[i], y_pos[i]] = False
                    grid[x_pos[i], (y_pos[i]+1) % gridsize] = True
                    y_pos[i] = (y_pos[i]+1) % gridsize
                    y_diff[i] += 1
                else:
                    fail += 1
            else:
                print('probleeme')

    else:
        random_choices = rng.choice(nparticles, size=nparticles)

        for l in range(nparticles):
            i = random_choices[l]

            if directions[i] < 0.25:
                if grid[(x_pos[i]+1) % gridsize, y_pos[i]] == False:
                    grid[x_pos[i], y_pos[i]] = False
                    grid[(x_pos[i]+1) % gridsize, y_pos[i]] = True
                    x_pos[i] = (x_pos[i]+1) % gridsize
                    x_diff[i] += 1
                else:
                    fail += 1

            elif directions[i] < 0.5:
                if grid[x_pos[i], (y_pos[i]-1) % gridsize] == False:
                    grid[x_pos[i], y_pos[i]] = False
                    grid[x_pos[i], (y_pos[i]-1) % gridsize] = True
                    y_pos[i] = (y_pos[i]-1) % gridsize
                    y_diff[i] -= 1
                else:
                    fail += 1

            elif directions[i] < 0.75:
                if grid[(x_pos[i]-1) % gridsize, y_pos[i]] == False:
                    grid[x_pos[i], y_pos[i]] = False
                    grid[(x_pos[i]-1) % gridsize, y_pos[i]] = True
                    x_pos[i] = (x_pos[i]-1) % gridsize
                    x_diff[i] -= 1
                else:
                    fail += 1

            elif directions[i] > 0.75:
                if grid[x_pos[i], (y_pos[i]+1) % gridsize] == False:
                    grid[x_pos[i], y_pos[i]] = False
                    grid[x_pos[i], (y_pos[i]+1) % gridsize] = True
                    y_pos[i] = (y_pos[i]+1) % gridsize
                    y_diff[i] += 1
                else:
                    fail += 1
            else:
                print('probleeme')

    # print(fail/nparticles)

    return grid, x_pos, y_pos, x_diff, y_diff



def print_info(gridsize, nparticles, tsteps, random_evo):

    t_limit = (1.0*gridsize/2)**2
    print(
f'''
#------------------------------------------------------------------------------
gridsize   = {gridsize}
nparticles = {nparticles}
tsteps     = {tsteps}
t_limit    = {t_limit}
random_evo = {random_evo}
#------------------------------------------------------------------------------
''')

# ALLORA, NON DEVI CONFRONTARE LA POSIZIONE ATTUALE CON QUELLA INIZIALE

# DEVI REGISTRARE I MOVIMENTI FATTI AGGIUNGENDO AD OGNI TIMESTEP LE MOSSE


# compute average movement already done at some time
def displacement(size, nparticles, x_diff, y_diff):

    current_mean_r2 = ( np.sum(x_diff**2 + y_diff**2) ) /nparticles

    return current_mean_r2


def displacement_sigma(size, nparticles, x_diff, y_diff):

    current_mean_r2 = ( np.sum(x_diff**2 + y_diff**2) ) /nparticles

    current_mean_r22 = ( np.sum((x_diff**2 + y_diff**2)**2) ) /nparticles

    # breakpoint()

    return (current_mean_r22 - current_mean_r2**2)**0.5


#------------------------------------------------------------------------------
def single_evo(initial_output=True):

    size = 20
    if size**2 > np.iinfo(np.int32).max:
        print('need to change int data type, grid is too large')
        sys.exit()

    nparticles = int(0.03*size**2)

    tsteps = int((size/2)**2 *3)

    random_evo = False

    progress_update = 1.0*tsteps/10

    if initial_output:
        print_info(size, nparticles, tsteps, random_evo)

    grid, x_pos, y_pos = make_initial_grid(size, nparticles)

    x_diff = np.zeros(nparticles)
    y_diff = np.zeros(nparticles)

    mean_r2 = np.zeros(tsteps)
    sigma_r2 = np.zeros(tsteps)


    for k in range(tsteps):
        if k % progress_update == 0:
            # can't format the string with numba...
            print(f'{k} / {tsteps} steps')
        grid, x_pos, y_pos, x_diff, y_diff = iterator(size, nparticles, grid, 
                                                x_pos, y_pos, x_diff, y_diff, 
                                                random_evo)
        mean_r2[k] = displacement(size, nparticles, x_diff, y_diff)
        sigma_r2[k] = displacement_sigma(size, nparticles, x_diff, y_diff)



    # fig, ax = plt.subplots()
    # fig.suptitle('R2')

    # ax.plot(range(tsteps), mean_r2)
    # ax.axvline((1.0*size/2)**2, color='red', label='limit')

    # model = LinearRegression(fit_intercept=True).fit(np.arange(0, (1.0*size/2)**2).reshape((-1, 1)), mean_r2[:int((1.0*size/2)**2)])
    # y = np.arange(0, tsteps)*model.coef_
    # ax.plot(np.arange(0, tsteps), y, linewidth=1, label='%.3f'%(model.coef_)+'x')

    # ax.grid()
    # fig.legend()
    # fig.show()


    fig, ax = plt.subplots()
    # fig.suptitle('D')

    # ax.plot(range(tsteps), mean_r2/(2*2*np.linspace(1, tsteps, tsteps)))
    ax.errorbar(range(tsteps), mean_r2/(2*2*np.linspace(1, tsteps, tsteps)), 
        yerr=sigma_r2/(2*2*np.linspace(1, tsteps, tsteps)),
        ecolor='green', elinewidth=1, capsize=5, capthick=2)
    ax.axvline((1.0*size/2)**2, color='red', label='t limit')

    ax.set_ylabel(r'$D(t)$')
    ax.set_xlabel(r'$t(\rm ns)$')

    ax.grid()
    fig.legend()
    fig.show()


    diffusion = mean_r2/(2*2*np.linspace(1, tsteps, tsteps))

    stupid = np.zeros((tsteps-1))

    for l in range(1, tsteps):
        stupid[l-1] = np.mean(diffusion[:l])

    fig, ax = plt.subplots()
    # fig.suptitle('STUPID')

    ax.plot(range(1, tsteps), stupid)
    ax.axvline((1.0*size/2)**2, color='red', label='limit')

    ax.set_ylabel(r'$\langle D(t) \rangle_T$')
    ax.set_xlabel(r'$T(\rm ns)$')

    ax.grid()
    fig.legend()
    fig.show()


    input('Enter when done...')


    return 1

#------------------------------------------------------------------------------
def multiple_evos(initial_output=True):

    size = 20
    if size**2 > np.iinfo(np.int32).max:
        print('need to change int data type, grid is too large')
        sys.exit()

    nparticles = int(0.03*size**2)

    tsteps = int((size/2)**2 *1)

    random_evo = False

    n_evos = 5

    progress_update = 1.0*n_evos/10

    if initial_output:
        print_info(size, nparticles, tsteps, random_evo)

    moron = np.zeros(n_evos)
    
    fig, ax = plt.subplots()

    for p in range(n_evos):
        if p % progress_update == 0:
            print(f'{p} / {n_evos} runs')
        
        grid, x_pos, y_pos = make_initial_grid(size, nparticles)

        x_diff = np.zeros(nparticles)
        y_diff = np.zeros(nparticles)

        mean_r2 = np.zeros(tsteps)

        for k in range(tsteps):

            grid, x_pos, y_pos, x_diff, y_diff = iterator(size, nparticles, grid, 
                                                    x_pos, y_pos, x_diff, y_diff, 
                                                    random_evo)
            mean_r2[k] = displacement(size, nparticles, x_diff, y_diff)



        diffusion = mean_r2/(2*2*np.linspace(1, tsteps, tsteps))

        stupid = np.zeros((tsteps-1))

        for l in range(1, tsteps):
            stupid[l-1] = np.mean(diffusion[:l])

        # R PLOT
        # ax.plot(range(tsteps), mean_r2*2)
        # D PLOT
        ax.plot(range(1, tsteps), stupid)


        moron[p] = stupid[len(stupid)-1]

    # breakpoint()
    sigma_moron = (np.mean(moron**2) - (np.mean(moron))**2)**0.5

    print('Diffusion coeff. = ', np.mean(moron),' +- ', sigma_moron)


    # fig.suptitle('R2')
    ax.axvline((1.0*size/2)**2, color='red', label='t limit')

    # R2 PLOT
    # model = LinearRegression(fit_intercept=True).fit(np.arange(0, (1.0*size/2)**2).reshape((-1, 1)), mean_r2[:int((1.0*size/2)**2)])
    # y = np.arange(0, tsteps)*model.coef_
    # ax.plot(np.arange(0, tsteps), y, linewidth=1, label='%.3f'%(model.coef_)+'x')
    # ax.plot(np.arange(0, tsteps), 2*np.arange(0, tsteps))

    # ax.set_ylabel(r'$\langle R^2 (t) \rangle (\rm \AA)$')
    # ax.set_xlabel(r'$t(\rm ns)$')

    #D PLOT
    ax.set_ylabel(r'$\langle D(t) \rangle_T$')
    ax.set_xlabel(r'$T(\rm ns)$')

    ax.grid()
    fig.legend()
    fig.show()
    input('Enter when done...')


    return 1

#------------------------------------------------------------------------------

try:
    # cProfile.run('main()', sort='cumtime')
    # single_evo()
    multiple_evos()
except:
    import traceback, pdb, sys
    traceback.print_exc()
    pdb.post_mortem()
    sys.exit(1)



# COME POSSO CALCOLARE IL DISPLACEMENT TENENDO CONTO CHE LE PARTICELLE
# RIAPPAIONO AL LATO OPPOSTO DELLA GRIGLIA GRAZIE ALLE PBC?
