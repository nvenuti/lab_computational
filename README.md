# Computational Physics Lab


Homeworks and exam project of class in Monte-Carlo computational methods

## HW1

Unit 4: Random walker simulations

Unit 5: Numerical integration with Metropolis algorithm


## HW2

Unit 7 : Simulation of Ising model with Metropolis algorithm

 ## HW3

 Unit 9 : Diffusion in lattice gas model


 ## Exam

 Nucleation in the Ising model
