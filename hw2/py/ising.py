import matplotlib.pyplot as plt
import numpy as np
import math as m
import sys
import cProfile
from sklearn.linear_model import LinearRegression


plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18

#from numba import njit


# PROBABILITÀ DELLE TRANSIZIONI HA UN PROBLEMA



rng = np.random.default_rng(seed=42424)
#------------------------------------------------------------------------------
# magnetizaton per spin
def magnetization_ps(grid, nspins):
    return np.sum(grid)/nspins

# energy per spin
def energy_ps(grid, gridsize):

    es = 0

    # parse all grid in lines
    for k in range(gridsize):
        for j in range(gridsize):
            # np.arange(0, gridsize, dtype=np.int32), k*np.ones(gridsize, dtype=np.int32) 
            es -= grid[k, j] * dearest_sum(gridsize, grid, k, j)

    return np.sum(es)/2 /gridsize**2

#  PER ORA NON LO METTIAMO IN UNA FUNZIONE DEDICATA
# def heat_capacity_ps(spins, nspins, nsteps):



#------------------------------------------------------------------------------
#initialize grid with random occupation
def make_initial_grid(gridsize, create_option='random'):

    if create_option == 'random':
        # generate RANDOM spins +-1
        grid = 2*rng.choice(2, size=(gridsize, gridsize))-1

    # if you're dumb and want more options...
    elif create_option == '1':
        grid = np.ones((gridsize, gridsize), dtype=np.int32)
    elif create_option == '0':
        grid = np.zeros((gridsie, gridsize), dtype=np.int32)
    elif create_option == 'chess':
        print('machechesss')

    return grid

# either loop over the particles or make n random choices,
# then attempt move in random direction
def iterator(gridsize, T, grid, tsteps):

    random_choices = np.zeros((gridsize**2, 2), dtype=np.int32)

    mag_array = np.zeros(tsteps)
    eng_array = np.zeros(tsteps)

    progress_update = 1.0*tsteps/10

    mag_array[0] = magnetization_ps(grid, gridsize**2)
    eng_array[0] = energy_ps(grid, gridsize)

    for k in range(1, tsteps):
        if k % progress_update == 0:
            # can't format the string with numba...
            print(f'{k} / {tsteps} steps')

        mag_array[k] = mag_array[k-1]
        eng_array[k] = eng_array[k-1]

        # average of 1 flip per spin at every timestep

        random_choices[:, 0] = rng.choice(gridsize, size=gridsize**2)
        random_choices[:, 1] = rng.choice(gridsize, size=gridsize**2)

        for i in range(gridsize**2):
            current_pos = random_choices[i]

            # compute all the neighbour spins of every random choice
            neighbourhood = dearest_sum(gridsize, grid, current_pos[0], current_pos[1], True)

            e_delta = 2* grid[current_pos[0], current_pos[1]] * neighbourhood

            # compute boltzmann factor and adjust for favourable dE
            # w = np.minimum(1, np.exp(-e_deltas/T))
            w = m.exp(-1.0*e_delta/T)

            # compute acceptance with Metropolis rule
            #--------------------------------------------------------------------------
            random_accept = rng.random()

            # DUMB VERSION FOR NOW
            accept_ratio = 0
            if w > random_accept:
                accept_ratio += 1
                grid[current_pos[0], current_pos[1]] *= -1

                eng_array[k] += e_delta/2/gridsize**2
                mag_array[k] += 2*grid[current_pos[0], current_pos[1]]/gridsize**2
            #--------------------------------------------------------------------------
            
            accept_ratio = accept_ratio/(gridsize**2)
        # # print(accept_ratio)
        # # print(random_choices)
        # print(grid)
        # print(np.where( w<random_accept) )
        # # print(e_deltas)
        # # # print(w)
        # a=input()
        # if a == 'stop':
        #     breakpoint()


    # random_choices = np.zeros((gridsize**2, 2), dtype=np.int32)

    # mag_array = np.zeros(tsteps)
    # eng_array = np.zeros(tsteps)

    # progress_update = 1.0*tsteps/10

    # mag_array[0] = magnetization_ps(grid, gridsize**2)
    # eng_array[0] = energy_ps(grid, gridsize)

    # for k in range(1, tsteps):
    #     if k % progress_update == 0:
    #         # can't format the string with numba...
    #         print(f'{k} / {tsteps} steps')

    #     mag_array[k] = mag_array[k-1]
    #     eng_array[k] = eng_array[k-1]

    #     # average of 1 flip per spin at every timestep
    #     random_choices[:, 0] = rng.choice(gridsize, size=gridsize**2)
    #     random_choices[:, 1] = rng.choice(gridsize, size=gridsize**2)

    #     # compute all the neighbour spins of every random choice
    #     neighbourhood = dearest_sum(gridsize, grid, random_choices, True)

    #     e_deltas = 2* grid[random_choices[:, 0], random_choices[:, 1]] * neighbourhood

    #     # compute boltzmann factor and adjust for favourable dE
    #     # w = np.minimum(1, np.exp(-e_deltas/T))
    #     w = np.exp(-1.0*e_deltas/T)

    #     # compute acceptance with Metropolis rule
    #     #--------------------------------------------------------------------------
    #     random_accept = rng.random(gridsize**2)

    #     # DUMB VERSION FOR NOW
    #     accept_ratio = 0
    #     for p in range(gridsize**2):
    #         if w[p] > random_accept[p]:
    #             accept_ratio += 1
    #             grid[random_choices[p, 0], random_choices[p, 1]] *= -1

    #             eng_array[k] += e_deltas[p]/gridsize**2
    #             mag_array[k] += 2*grid[random_choices[p, 0], random_choices[p, 1]]/gridsize**2
    #     #--------------------------------------------------------------------------
    #     accept_ratio = accept_ratio/(gridsize**2)
    #     # print(accept_ratio)
    #     # print(random_choices)
    #     print(grid)
    #     print(np.where( w<random_accept) )
    #     # print(e_deltas)
    #     # # print(w)
    #     a=input()
    #     if a == 'stop':
    #         breakpoint()

    return grid, mag_array, eng_array


# vectorized computation of neighbourhood spins
def dearest_sum(gridsize, grid, x_pos, y_pos, output=False):

    x_p = (x_pos+1) % (gridsize)
    x_m = (x_pos-1) % (gridsize)
    y_p = (y_pos+1) % (gridsize)
    y_m = (y_pos-1) % (gridsize)

    dearest = grid[x_p, y_pos] + grid[x_m, y_pos] + grid[x_pos, y_p] + grid[x_pos, y_m]


    # x_pos = random_pos[:, 0]
    # y_pos = random_pos[:, 1]
    # dearest = np.zeros((len(x_pos)), dtype=np.int32)

    # x_p = (x_pos+1) % (gridsize)
    # x_m = (x_pos-1) % (gridsize)
    # y_p = (y_pos+1) % (gridsize)
    # y_m = (y_pos-1) % (gridsize)

    # dearest = grid[x_p, y_pos] + grid[x_m, y_pos] + grid[x_pos, y_p] + grid[x_pos, y_m]

    # if output:
    #     breakpoint()
    #     print('dearest= ', dearest)
    #     print(x_m, x_pos, x_p)
    #     print(y_m, y_pos, y_p)


    return dearest



def print_info(gridsize, equisteps, evosteps, temperature):
    print(
f'''
#------------------------------------------------------------------------------
gridsize    = {gridsize}
equisteps   = {equisteps}
evosteps    = {evosteps}
Temp.       = {temperature}
#------------------------------------------------------------------------------
''')

#------------------------------------------------------------------------------
def single_evo(initial_output=True, save_file=False):

    size = 30
    if size**2 > np.iinfo(np.int32).max:
        print('need to change int data type, grid is too large')
        sys.exit()

    nspins = size**2

    equisteps = 1000

    evosteps = 1000

    T = 2

    if initial_output:
        print_info(size, equisteps, evosteps, T)

    grid = make_initial_grid(size)

    # equillibration of the system
    print('Equilibration...')
    grid, mag_array, eng_array = iterator(size, T, grid, equisteps)

    print('Evolution...')


    fig, ax = plt.subplots()
    # fig.suptitle('R2')

    ax.plot(range(equisteps), mag_array)

    ax.set_ylabel(r'$M/N$')
    ax.set_xlabel(r'$t$')

    ax.grid()
    # fig.legend()
    fig.show()


    fig, ax = plt.subplots()
    # fig.suptitle('R2')

    ax.plot(range(equisteps), eng_array)

    ax.set_ylabel(r'$E/N$')
    ax.set_xlabel(r'$t$')

    ax.grid()
    # fig.legend()
    fig.show()

    if save_file:
        np.savetxt('mag_equi_1.txt', mag_array)
        np.savetxt('eng_equi_1.txt', eng_array)

    input('Enter when done...')


    return 1


#------------------------------------------------------------------------------
def multiple_evos(initial_output=True, save_file=False):

    size = 30
    if size**2 > np.iinfo(np.int32).max:
        print('need to change int data type, grid is too large')
        sys.exit()

    nspins = size**2

    equisteps = 1000

    evosteps = 1000

    n_evos = 5

    T = 2

    if initial_output:
        print_info(size, equisteps, evosteps, T)

    for p in range(n_evos):
        grid = make_initial_grid(size)

        # equillibration of the system
        print('Equilibration...')
        grid, mag_array, eng_array = iterator(size, T, grid, equisteps)

    print('Evolution...')


    fig, ax = plt.subplots()
    # fig.suptitle('R2')

    ax.plot(range(equisteps), mag_array)

    ax.set_ylabel(r'$M/N$')
    ax.set_xlabel(r'$t$')

    ax.grid()
    # fig.legend()
    fig.show()


    fig, ax = plt.subplots()
    # fig.suptitle('R2')

    ax.plot(range(equisteps), eng_array)

    ax.set_ylabel(r'$E/N$')
    ax.set_xlabel(r'$t$')

    ax.grid()
    # fig.legend()
    fig.show()

    if save_file:
        np.savetxt('mag_equi_1.txt', mag_array)
        np.savetxt('eng_equi_1.txt', eng_array)

    input('Enter when done...')

    return 1
#------------------------------------------------------------------------------

try:
    initial_output = True
    save_file = True
    # cProfile.run('main()', sort='cumtime')
    single_evo(initial_output, save_file)
except:
    import traceback, pdb, sys
    traceback.print_exc()
    pdb.post_mortem()
    sys.exit(1)



# COME POSSO CALCOLARE IL DISPLACEMENT TENENDO CONTO CHE LE PARTICELLE
# RIAPPAIONO AL LATO OPPOSTO DELLA GRIGLIA GRAZIE ALLE PBC?
