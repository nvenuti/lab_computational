import matplotlib.pyplot as plt
import numpy as np
import math as m
import sys
import time
import cProfile
from sklearn.linear_model import LinearRegression
from pathlib import Path

# import multiprocessing

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18

from old_isingf import old_isingf as isingf

rng = np.random.default_rng(seed=42424)


#------------------------------------------------------------------------------

def plot_single(eq_data, ev_data, tem, spins):

    fig, axs = plt.subplots(ncols=2, nrows=1,layout="constrained", figsize=(15, 10))
    fig.suptitle('Equilibration, T ='+str(tem))
    axs[0].plot(eq_data[:, 0], c='orange',label=r'$E/N$')
    axs[0].set_ylim([-2,0])
    if bc == 'obc':
        axs[0].plot(range(N_eq), -2*(1-1/gridsize)*np.ones(N_eq), '--')
    axs[1].plot(eq_data[:, 1], label=r'$M/N$')
    axs[1].set_ylim([-1, 1])

    for ax in axs:
        ax.legend()
        ax.grid()
        ax.set_xlabel('MC steps')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("single_eq_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("single_eq_"+str(file_id)+".png")
    plt.savefig(file_path)


    fig, axs = plt.subplots(ncols=2, nrows=1,layout="constrained", figsize=(15, 10))
    fig.suptitle('Evolution, T ='+str(tem))
    axs[0].plot(ev_data[:, 0], c='orange',label=r'$E/N$')
    axs[0].set_ylim([-2,0])
    if bc == 'obc':
        axs[0].plot(range(N_mc), -2*(1-1/gridsize)*np.ones(N_mc), '--')
    axs[1].plot(ev_data[:, 1],label=r'$M/N$')
    axs[1].set_ylim([-1, 1])

    for ax in axs:
        ax.legend()
        ax.grid()
        ax.set_xlabel('MC steps')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("single_ev_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("single_ev_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)


    fig, ax = plt.subplots(layout="constrained", figsize=(10, 10))
    fig.suptitle('Final state, T ='+str(tem))

    ax.imshow(spins)

    # ax.legend()
    ax.grid()
    # ax.set_xlabel('MC steps')

    plt.savefig('snapshot.png')



#plotting as a function of temperature
def plot_temps(T_list,res):
    # breakpoint()
    fig, axs = plt.subplots(ncols=2, nrows=2,layout="constrained", figsize=(15, 10))
    # axs[0,0].errorbar(T_list,res[:,0], yerr=res[:,4],c='orange',label='energy', capsize=5, capthick=2)
    # axs[0,1].errorbar(T_list,res[:,1], yerr=res[:,5],label='magnetisation', capsize=5, capthick=2)
    axs[0,0].plot(T_list,res[:,0], 'o-',c='orange',label='energy')
    axs[0,1].plot(T_list,res[:,1], 'o-',label='magnetisation')
    axs[1,0].plot(T_list,res[:,2], 'o-',c='red',label='c (fluctuations)')
    axs[1,1].plot(T_list,res[:,3], 'o-',c='k',label='magnetic susceptibility')
    # numerical specific heat
    midpoints = (T_list[:len(T_list)-1] + T_list[1:])/2
    derivs = (res[1:, 0] - res[:len(res[:, 0])-1, 0])/(T_list[1:]-T_list[:len(T_list)-1])
    axs[1,0].plot(midpoints, derivs,'x-',c='magenta',label='c (E derivative)')
    for axx in axs:
        for ax in axx:
            ax.grid()
            ax.legend()
            ax.set_xlabel('T')
            ax.set_xlim([T_min, T_max])
            ax.axvline(x=2.26,c='grey')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("multirun_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("multirun_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)

# useful function recycled from the Python class for the Ising model

def get_up_right(i,j):
      
    if j==(gridsize-1):
        right = 0
    else:
        right = j+1
    if i==(gridsize-1):
        up = 0
    else:
        up = i-1 

    return up,right


def calc_energy(L,spin):
    '''
    Calculates the total energy.
    NB: this is used only at initialization.
    '''
    E = 0
    if bc == 'pbc':
        for i in range(L):
            for j in range(L):
                up,right = get_up_right(i,j)
                E = E - spin[i,j]*(spin[up, j]+spin[i,right])
    elif bc == 'obc':
        for i in range(1, L):
            for j in range(L-1):
                up,right = get_up_right(i,j)
                E = E - spin[i,j]*(spin[up, j]+spin[i,right])
        for j in range(L-1):
            E = E - spin[0,j]*(spin[0,j+1])

        for i in range(1, L):
            E = E - spin[i,L-1]*(spin[i-1,L-1])

    return E

def get_stats(data,N,T, mute=True):
        '''
        Calculate, store and print 
        statistical quantities
        '''
        E_N_av = np.average(data[:,0])/N
        M_N_av = np.average(np.abs(data[:,1]))/N
        c_heat = ( np.average(data[:,0]**2)- (E_N_av*N)**2 )/(T**2*N)
        chi_mag = ( np.average(data[:,1]**2)- (M_N_av*N)**2 )/(T*N)
        # sigma2_m = ( np.mean(data[:,1]**2) - (M_N_av*N)**2 )
        # sigma2_e = ( np.mean(data[:,0]**2) - (E_N_av*N)**2 )

        #print("Total number of steps run = {}".format(counter/N))
        #print("Acceptance ratio = {}".format(ar))
        if mute:
            pass
        else:
            print("Temperature = {}".format(T))
            print("<E/N> = {}".format(E_N_av))
            print("<M/N> = {}".format(M_N_av))
            print("specific heat = {}".format(c_heat))
            print("magnetic susceptibility = {}".format(chi_mag))
            print(sigma2_e**0.5, sigma2_m**0.5)
        return E_N_av,M_N_av,c_heat,chi_mag #, (sigma2_e**0.5)/N, (sigma2_m**0.5)/N
#------------------------------------------------------------------------------

gridsize = 30

N_eq = 1000
N_mc = 200000
T_min = 1.5
T_max = 3
T_list = np.linspace(T_min, T_max, 40)

n_evos = 20

bc = 'pbc'

def main(single_temp='False'):

    start = time.time()

    print('bc =', bc)

    isingf.l = gridsize
    isingf.n = gridsize**2

    isingf.alloc()

    if len(sys.argv) == 3:
        single_temp = sys.argv[1]
        T = float(sys.argv[2])

    if single_temp == 'True':
        # isingf.seed = np.randint(1, 10000)
        print("Temperature = {}".format(T))
        isingf.t  = T

        isingf.init()
        isingf.spin = np.ones((gridsize, gridsize), dtype=np.int32, order='F')
        # isingf.spin[:, :int(gridsize/2)] *= -1
        #isingf.initial_grid()

        isingf.e = calc_energy(isingf.l,isingf.spin)
        isingf.m = np.sum(isingf.spin)

        eq_data = np.zeros((N_eq,2))
        ev_data = np.zeros((N_mc,2))

        if bc =='pbc':

            for i in range(N_eq):
                isingf.metropolis()
                eq_data[i,:] = (isingf.e,isingf.m)
            for i in range(N_mc):
                isingf.metropolis()
                ev_data[i,:] = (isingf.e,isingf.m)

        elif bc == 'obc':
            for i in range(N_eq):
                isingf.metropolis_obc()
                eq_data[i,:] = (isingf.e,isingf.m)
            for i in range(N_mc):
                isingf.metropolis_obc()
                ev_data[i,:] = (isingf.e,isingf.m)


        eq_data = eq_data/isingf.n
        ev_data = ev_data/isingf.n


        stop = time.time()
        print(stop-start)

        plot_single(eq_data, ev_data, T, isingf.spin)


    else:

        res_array = np.zeros((n_evos, len(T_list), 4))
        res_f = np.zeros((len(T_list), 6))

        for k in range(n_evos):
            for l in range(len(T_list)):
                # isingf.seed = np.randint(1, 10000)
                isingf.t  = T_list[l]

                #isingf.alloc()
                isingf.init()
                isingf.spin = np.ones((gridsize, gridsize), dtype=np.int32,order='F')
                isingf.initial_grid()
                isingf.e = calc_energy(isingf.l,isingf.spin)
                isingf.m = np.sum(isingf.spin)
                data=np.zeros((N_mc,2))


                if bc =='pbc':
                    for i in range(N_eq):
                        isingf.metropolis()
                    for i in range(N_mc):
                        isingf.metropolis()
                        data[i,:] = (isingf.e,isingf.m)

                elif bc == 'obc':
                    for i in range(N_eq):
                        isingf.metropolis_obc()
                    for i in range(N_mc):
                        isingf.metropolis_obc()
                        data[i,:] = (isingf.e,isingf.m)


                e,m,heat,chi = get_stats(data,isingf.n,isingf.t)

                res_array[k, l, 0] = e
                res_array[k, l, 1] = m
                res_array[k, l, 2] = heat
                res_array[k, l, 3] = chi

            # print('done T = ', T)
            print(k)

        for i in range(len(T_list)):
            res_f[i, 0] = np.mean(res_array[:, i, 0])
            res_f[i, 1] = np.mean(res_array[:, i, 1])
            res_f[i, 2] = np.mean(res_array[:, i, 2])
            res_f[i, 3] = np.mean(res_array[:, i, 3])
            # res_f[i, 4] = (np.mean(res_array[:, i, 0]**2) - res_f[i, 0]**2)**0.5
            # res_f[i, 5] = (np.mean(res_array[:, i, 1]**2) - res_f[i, 1]**2)**0.5


        # print('L = ', gridsize, np.amax(res_f[:, 2]), np.amax(res_f[:, 3]))
        plot_temps(T_list,res_f)

        #    del isingf



#------------------------------------------------------------------------------

try:
    main()
except:
    import traceback, pdb, sys
    traceback.print_exc()
    pdb.post_mortem()
    sys.exit(1)