import numpy as np
import math as m
import sys
import time
import cProfile
from pathlib import Path
import getpass

# import multiprocessing

from isingf import isingf
# from old_isingf import old_isingf as isingf

from funcs import *

def main(single_temp='False'):

    print('bc =', bc)

    isingf.l = gridsize
    isingf.n = gridsize**2
    isingf.nmcs   = N_mc
    isingf.nequil = N_eq
    isingf.alloc()


    if len(sys.argv) == 3:
        single_temp = sys.argv[1]
        T = float(sys.argv[2])

    if single_temp == 'True':
        # isingf.seed = np.randint(1, 10000)
        print("Temperature = {}".format(T))
        isingf.t  = T
        isingf.init()

        make_initial(initial_choice)

        eq_data = np.zeros((N_eq,2))
        ev_data = np.zeros((N_mc,2))

        if bc =='pbc':

            if not stopmotion:
                isingf.iterator(0)
                eq_data[:, 0] = isingf.e_eq
                eq_data[:, 1] = isingf.m_eq

                isingf.iterator(1)
                ev_data[:, 0] = isingf.e_evo
                ev_data[:, 1] = isingf.m_evo

            else:
                eq_data[0, 0] = isingf.e_ini
                eq_data[0, 1] = isingf.m_ini

                fig, ax = plt.subplots(layout="constrained")#, figsize=(10, 10))
                ax.set_xlim(-0.5, gridsize-0.5)
                ax.set_ylim(-0.5, gridsize-0.5)
                background = fig.canvas.copy_from_bbox(ax.bbox)
                
                # fig.suptitle('0')
                pl = ax.imshow(isingf.spin, vmin=-1, vmax=1)
                fig.show()
                input()

                for i in range(1, N_eq):
                    eq_data[i, 0], eq_data[i, 1] = isingf.metropolis(eq_data[i-1, 0], eq_data[i-1, 1])
                    
                    fig.canvas.restore_region(background)
                    # fig.suptitle(str(i))
                    pl.set_data(isingf.spin)
                    ax.draw_artist(pl)
                    fig.canvas.blit(ax.bbox)
                    # fig.canvas.draw()
                    if i%(1.0*N_eq/100) == 0:
                        print(i, end="\r", flush=True)
                        print()
                    input("\033[F")


                ev_data[0, 0] = eq_data[N_eq-1, 0]
                ev_data[0, 1] = eq_data[N_eq-1, 1]
                for i in range(1, N_mc):
                    ev_data[i, 0], ev_data[i, 1] = isingf.metropolis(ev_data[i-1, 0], ev_data[i-1, 1])


        elif bc == 'obc':

            if not stopmotion:
                isingf.iterator_obc(0)
                eq_data[:, 0] = isingf.e_eq
                eq_data[:, 1] = isingf.m_eq

                isingf.iterator_obc(1)
                ev_data[:, 0] = isingf.e_evo
                ev_data[:, 1] = isingf.m_evo
            
            else:
                eq_data[0, 0] = isingf.e_ini
                eq_data[0, 1] = isingf.m_ini

                fig, ax = plt.subplots(layout="constrained")#, figsize=(10, 10))
                fig.suptitle('0')
                ax.set_xlim(-0.5, gridsize-0.5)
                ax.set_ylim(-0.5, gridsize-0.5)

                background = fig.canvas.copy_from_bbox(ax.bbox)

                pl = ax.imshow(isingf.spin, vmin=-1, vmax=1)
                fig.show()
                input()

                for i in range(1, N_eq):
                    eq_data[i, 0], eq_data[i, 1] = isingf.metropolis_obc(eq_data[i-1, 0], eq_data[i-1, 1])
                    
                    fig.suptitle(str(i))
                    pl.set_data(isingf.spin)

                    fig.canvas.restore_region(background)
                    ax.draw_artist(pl)
                    fig.canvas.blit(ax.bbox)
                    # fig.canvas.draw()
                    if i%(1.0*N_eq/100) == 0:
                        print(i, end="\r", flush=True)
                        print()
                    input("\033[F")

                ev_data[0, 0] = eq_data[N_eq-1, 0]
                ev_data[0, 1] = eq_data[N_eq-1, 1]
                for i in range(1, N_mc):
                    ev_data[i, 0], ev_data[i, 1] = isingf.metropolis_obc(ev_data[i-1, 0], ev_data[i-1, 1])



        eq_data = eq_data/isingf.n
        ev_data = ev_data/isingf.n

        plot_single(eq_data, ev_data, T, isingf.spin)


    else:

        res_array = np.zeros((n_evos, len(T_list), 4))
        res_f = np.zeros((len(T_list), 8))
        ev_data = np.zeros((N_mc,2))

        for k in range(n_evos):
            for l in range(len(T_list)):

                # isingf.seed = np.randint(1, 10000)
                isingf.t  = T_list[l]
                isingf.init()

                if l == 0:
                    make_initial(initial_choice)
                else:
                    make_initial(initial_choice, reuse_final_state)

                # if we are using multiple temps we don't care about the equilibration 
                if bc =='pbc':
                    if l>0 and reuse_final_state:
                        pass
                    else:
                        isingf.iterator(0)

                    isingf.iterator(1)
                    ev_data[:, 0] = isingf.e_evo
                    ev_data[:, 1] = isingf.m_evo

                elif bc == 'obc':
                    if l>0 and reuse_final_state:
                        pass
                    else:
                        isingf.iterator_obc(0)

                    isingf.iterator_obc(1)
                    ev_data[:, 0] = isingf.e_evo
                    ev_data[:, 1] = isingf.m_evo

                e,m,heat,chi = get_stats(ev_data,isingf.n,isingf.t)

                res_array[k, l, 0] = e
                res_array[k, l, 1] = m
                res_array[k, l, 2] = heat
                res_array[k, l, 3] = chi

                # print('done T = ', T)
            print(k, '/', n_evos)

        # compute the avrages over multiple evolutions
        for i in range(len(T_list)):
            res_f[i, 0] = np.mean(res_array[:, i, 0])
            res_f[i, 1] = np.mean(res_array[:, i, 1])
            res_f[i, 2] = np.mean(res_array[:, i, 2])
            res_f[i, 3] = np.mean(res_array[:, i, 3])
            res_f[i, 4] = (np.mean(res_array[:, i, 0]**2) - res_f[i, 0]**2)**0.5
            res_f[i, 5] = (np.mean(res_array[:, i, 1]**2) - res_f[i, 1]**2)**0.5
            res_f[i, 6] = (np.mean(res_array[:, i, 2]**2) - res_f[i, 2]**2)**0.5
            res_f[i, 7] = (np.mean(res_array[:, i, 3]**2) - res_f[i, 3]**2)**0.5


        plot_temps(T_list,res_f)

        # del isingf

#------------------------------------------------------------------------------

try:
    main()
except:
    import traceback, pdb, sys
    traceback.print_exc()
    pdb.post_mortem()
    sys.exit(1)



