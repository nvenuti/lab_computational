import matplotlib.pyplot as plt
import numpy as np

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18



fig, ax = plt.subplots(layout="constrained", figsize=(15, 10))

x = np.array([10., 15., 20., 25, 30.])
y_chi = np.array([2.47, 2.41, 2.3775, 2.347, 2.335])-2.269
y_c   = np.array([2.35, 2.325, 2.31, 2.3, 2.292])-2.269

x_th = np.array([10., 30.])
y_th = (x_th)**(-1)


ax.plot(x**1, y_chi**1, label=r'peak of $\chi$', marker='x', ms=10, c='black', lw=2)
ax.plot(x**1, y_c**1, label='peak of c (flucts.)', marker='x', ms=10, color='red', lw=2)


# ax.errorbar([4, 30], [2.80769231-2.269, 2.30769231+0.01875-2.269], yerr=[0.01875], label=r'peak of $\chi$', capsize=10, capthick=2, marker='x', ms=10, c='black', lw=2)
# ax.errorbar([4, 30], [2.46153846-2.269, 2.30769231-2.269], yerr=[0.01875], label='peak of c (flucts.)',capsize=10, capthick=2, marker='x', ms=10, color='red', lw=2)
# ax.errorbar([4, 30], [(2.21+2.628)/2-2.269, 2.34615385-2.269], yerr=[0.01875, (2.628-2.21)/2], label='peak of c (E deriv.)', capsize=10, capthick=2, marker='x', ms=10, color='magenta', lw=2)

# ax.errorbar([4], [2.80769231-2.269], yerr=[0.01875], label=r'peak of $\chi$', capsize=10, capthick=2, marker='x', ms=10, c='black')
# ax.errorbar([4], [2.46153846-2.269], yerr=[0.01875], label='peak of c (flucts.)', capsize=10, capthick=2, marker='x', ms=10, color='red')
# ax.errorbar([4], [(2.21+2.628)/2-2.269], yerr=[(2.628-2.21)/2], label='peak of c (E deriv.)', capsize=10, capthick=2, marker='x', ms=10, color='magenta')

ax.plot(x_th**1, y_th**1, label='scaling law', lw=3, linestyle='--')

ax.set_xlabel(r'$L$')
ax.set_ylabel(r'$T_c(L) - T_c^{th}$')

ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xticks(x)

ax.grid()
fig.legend()

plt.savefig('p1.png')