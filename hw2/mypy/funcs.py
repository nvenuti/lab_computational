import matplotlib.pyplot as plt
import numpy as np
import math as m
import sys
import time
import cProfile
from sklearn.linear_model import LinearRegression
from pathlib import Path

# import multiprocessing

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18

from isingf import isingf
# from old_isingf import old_isingf as isingf

from funcs import *


# yes we use some global variables

rng = np.random.default_rng(seed=42424)

gridsize = 30

N_eq = 1000
N_mc = 200000
T_min = 1.5
T_max = 3
T_list = np.linspace(T_min, T_max, 20)

n_evos = 10

bc = 'pbc'

initial_choice = '1'

reuse_final_state = True

stopmotion = False

#------------------------------------------------------------------------------

def plot_single(eq_data, ev_data, tem, spins):

    fig, axs = plt.subplots(ncols=2, nrows=1,layout="constrained", figsize=(15, 10))
    fig.suptitle('Equilibration, T ='+str(tem))
    axs[0].plot(eq_data[:, 0], c='orange',label=r'$E/N$')
    axs[0].set_ylim([-2,0])
    if bc == 'obc':
        axs[0].plot(range(N_eq), -2*(1-1/gridsize)*np.ones(N_eq), '--')
    axs[1].plot(eq_data[:, 1], label=r'$M/N$')
    axs[1].set_ylim([-1, 1])

    for ax in axs:
        ax.legend()
        ax.grid()
        ax.set_xlabel('MC steps')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("single_eq_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("single_eq_"+str(file_id)+".png")
    plt.savefig(file_path)


    fig, axs = plt.subplots(ncols=2, nrows=1,layout="constrained", figsize=(15, 10))
    fig.suptitle('Evolution, T ='+str(tem))
    axs[0].plot(ev_data[:, 0], c='orange',label=r'$E/N$')
    axs[0].set_ylim([-2,0])
    if bc == 'obc':
        axs[0].plot(range(N_mc), -2*(1-1/gridsize)*np.ones(N_mc), '--')
    axs[1].plot(ev_data[:, 1],label=r'$M/N$')
    axs[1].set_ylim([-1, 1])

    for ax in axs:
        ax.legend()
        ax.grid()
        ax.set_xlabel('MC steps')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("single_ev_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("single_ev_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)


    fig, ax = plt.subplots(layout="constrained", figsize=(10, 10))
    fig.suptitle('Final state after '+str(N_mc)+' steps, T ='+str(tem))

    ax.imshow(spins)

    plt.savefig('snapshot.png')



#plotting as a function of temperature
def plot_temps(T_list,res):
    # breakpoint()
    fig, axs = plt.subplots(ncols=2, nrows=2,layout="constrained", figsize=(15, 10))
    fig.suptitle('L = '+str(gridsize))
    axs[0,0].errorbar(T_list,res[:,0], yerr=res[:,4],c='orange',label='energy', capsize=5, capthick=2)
    axs[0,1].errorbar(T_list,res[:,1], yerr=res[:,5],label='magnetisation', capsize=5, capthick=2)
    axs[1,0].errorbar(T_list,res[:,2], yerr=res[:,6],c='red',label='c (fluctuations)', capsize=5, capthick=2)
    axs[1,1].errorbar(T_list,res[:,3], yerr=res[:,7],c='black', label='magnetic susceptibility', capsize=5, capthick=2)
 

    # axs[0,0].plot(T_list,res[:,0], 'o-',c='orange',label='energy')
    # axs[0,1].plot(T_list,res[:,1], 'o-',label='magnetisation')
    # axs[1,0].plot(T_list,res[:,2], 'o-',c='red',label='c (fluctuations)')
    # axs[1,1].plot(T_list,res[:,3], 'o-',c='black',label='magnetic susceptibility')

    # numerical specific heat
    midpoints = (T_list[:len(T_list)-1] + T_list[1:])/2
    derivs = (res[1:, 0] - res[:len(res[:, 0])-1, 0])/(T_list[1:]-T_list[:len(T_list)-1])
    axs[1,0].plot(midpoints, derivs,'x-',c='magenta',label='c (E derivative)')
    for axx in axs:
        for ax in axx:
            ax.grid()
            ax.legend()
            ax.set_xlabel('T')
            ax.set_xlim([T_min, T_max])
            ax.axvline(x=2.26,c='grey')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("multirun_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("multirun_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)

# useful function recycled from the Python class for the Ising model
def get_up_right(i,j):
      
    if j==(gridsize-1):
        right = 0
    else:
        right = j+1
    if i==0:
        up = gridsize-1
    else:
        up = i-1

    return up,right


def calc_energy(L,spin):
    '''
    Calculates the total energy.
    NB: this is used only at initialization.
    '''
    E = 0
    if bc == 'pbc':
        for i in range(L):
            for j in range(L):
                up,right = get_up_right(i,j)
                E = E - spin[i,j]*(spin[up, j]+spin[i,right])

    elif bc == 'obc':
        for i in range(1, L):
            for j in range(L-1):
                up,right = get_up_right(i,j)
                E = E - spin[i,j]*(spin[up, j]+spin[i,right])
        for j in range(L-1):
            E = E - spin[0,j]*(spin[0,j+1])

        for i in range(1, L):
            E = E - spin[i,L-1]*(spin[i-1,L-1])

    return E

def get_stats(data,N,T, mute=True):
        '''
        Calculate, store and print 
        statistical quantities
        '''
        E_N_av = np.average(data[:,0])/N
        M_N_av = np.average(np.abs(data[:,1]))/N
        c_heat = ( np.average(data[:,0]**2)- (E_N_av*N)**2 )/(T**2*N)
        chi_mag = ( np.average(data[:,1]**2)- (M_N_av*N)**2 )/(T*N)
        # sigma2_m = ( np.mean(data[:,1]**2) - (M_N_av*N)**2 )
        # sigma2_e = ( np.mean(data[:,0]**2) - (E_N_av*N)**2 )

        #print("Total number of steps run = {}".format(counter/N))
        #print("Acceptance ratio = {}".format(ar))
        if mute:
            pass
        else:
            print("Temperature = {}".format(T))
            print("<E/N> = {}".format(E_N_av))
            print("<M/N> = {}".format(M_N_av))
            print("specific heat = {}".format(c_heat))
            print("magnetic susceptibility = {}".format(chi_mag))
            print(sigma2_e**0.5, sigma2_m**0.5)
        return E_N_av,M_N_av,c_heat,chi_mag #, (sigma2_e**0.5)/N, (sigma2_m**0.5)/N


def make_initial(initial_setting, reuse_setting=False):

    if reuse_setting:
        # we use as initial state the current array from isingf
        # compute macroscopic properties with current grid
        isingf.e_eq[isingf.nequil-1] = calc_energy(isingf.l,isingf.spin)
        isingf.m_eq[isingf.nequil-1] = np.sum(isingf.spin)
        return 1

    if initial_setting == '1':
        # initialize grid with all +1
        isingf.spin = np.ones((gridsize, gridsize), dtype=np.int32,order='F') 

    elif initial_setting == 'random':
        # initialize grid with all +1
        isingf.spin = np.ones((gridsize, gridsize), dtype=np.int32,order='F') 
        # then generate random spins
        isingf.initial_grid()

    elif initial_setting == '5050':
        # initialize grid with all +1
        isingf.spin = np.ones((gridsize, gridsize), dtype=np.int32,order='F')
        # flip left half of grid
        isingf.spin[:, :int(gridsize/2)] *= -1

    elif initial_setting == 'chess':
        # initialize grid with all +1
        isingf.spin = np.ones((gridsize, gridsize), dtype=np.int32,order='F')
        # make chessboard
        isingf.spin[::2, :] *= -1
        isingf.spin[:, ::2] *= -1

    else:
        print('Check \'initial_setting\' variable')
        sys.exit()

    # compute macroscopic properties with initial grid
    isingf.e_ini = calc_energy(isingf.l,isingf.spin)
    isingf.m_ini = np.sum(isingf.spin)

#------------------------------------------------------------------------------
