module isingf
  implicit none
  public :: init, initial_grid, alloc
  public :: metropolis, DeltaE, iterator
  public :: metropolis_obc, DeltaE_obc, iterator_obc
  !integer, public, dimension(8) :: seed
  integer, public :: L,N,nmcs,nequil
  real (kind = 8), public, dimension(-8:8) :: w
  integer, public, dimension(:,:), allocatable :: spin
  integer, public, dimension(:), allocatable :: E_eq, M_eq, E_evo, M_evo
  real (kind = 8), public :: T
  integer, public :: E_ini, M_ini
  integer, public :: accept
contains

  subroutine init()
    integer :: dE

    do dE = -8,8,1
      w(dE) = exp(-dE/T)
    end do
  end subroutine init


  subroutine alloc()
    allocate(spin(L,L))
    allocate(E_evo(nmcs))
    allocate(M_evo(nmcs))
    allocate(E_eq(nequil))
    allocate(M_eq(nequil))
  end subroutine alloc

  ! make random initial grid
  subroutine initial_grid()
    integer :: x, y
    real :: rnd

    do x = 1, L
      do y = 1, L
        call random_number(rnd)
        if (rnd >= 0.5) then
          spin(x, y) = 1
        else
          spin(x, y) = -1
        endif
      enddo
    enddo
  end subroutine initial_grid

  subroutine iterator(eq_or_evo)
    integer :: k
    integer, intent(in) :: eq_or_evo

    if (eq_or_evo == 0) then
        E_eq(1) = E_ini
        M_eq(1) = M_ini
        do k = 2, nequil
          call metropolis( E_eq(k-1), M_eq(k-1), E_eq(k), M_eq(k) )
        end do

    else if (eq_or_evo == 1) then
        E_evo(1) = E_eq(nequil)
        M_evo(1) = M_eq(nequil)
        do k = 2, nmcs
          call metropolis( E_evo(k-1), M_evo(k-1), E_evo(k), M_evo(k) )
        end do

    else
      print*, "Bad eq_or_evo"

    end if
  end subroutine iterator


  subroutine metropolis(E, M, new_E, new_M)
    !  one Monte Carlo step per spin
    integer :: ispin,x,y,dE
    integer, intent(in) :: E, M
    integer, intent(out) :: new_E, new_M
    real :: rnd

    new_E = E
    new_M = M

    do ispin = 1,N
       !     random x and y coordinates for trial spin
       call random_number(rnd)
       x = int(L*rnd) + 1
       call random_number(rnd)
       y = int(L*rnd) + 1
       dE = DeltaE(x,y)
       call random_number(rnd)
       if (rnd <= w(dE)) then
          spin(x,y) = -spin(x,y)
          accept = accept + 1
          new_M = new_M + 2*spin(x,y)  ! factor 2 is to account for the variation:
          new_E = new_E + dE           ! (-(-)+(+))
       
       end if
    end do
  end subroutine metropolis

  function DeltaE(x,y) result (DeltaE_result)
    !  periodic boundary conditions
    integer, intent (in) :: x,y
    integer :: DeltaE_result
    integer :: left
    integer :: right
    integer :: up
    integer :: down
    if (x == 1) then
       left = spin(L,y)
       right = spin(2,y)
    else if (x == L) then
       left = spin(L-1,y)
       right = spin(1,y)
    else
       left = spin(x-1,y)
       right = spin(x+1,y)
    end if
    if (y == 1) then
       up = spin(x,2)
       down = spin(x,L)
    else if (y == L) then
       up = spin(x,1)
       down = spin(x,L-1)
    else
       up = spin(x,y+1)
       down = spin(x,y-1)
    end if
    DeltaE_result = 2*spin(x,y)*(left + right + up + down)
! also here the factor 2 is to account for the variation
  end function DeltaE


    subroutine iterator_obc(eq_or_evo)
    integer :: k, eq_or_evo

    if (eq_or_evo == 0) then
        E_eq(1) = E_ini
        M_eq(1) = M_ini
        do k = 2, nequil
          call metropolis_obc( E_eq(k-1), M_eq(k-1), E_eq(k), M_eq(k) )
        end do

    else if (eq_or_evo == 1) then
        E_evo(1) = E_eq(nequil)
        M_evo(1) = M_eq(nequil)
        do k = 2, nmcs
          call metropolis_obc( E_evo(k-1), M_evo(k-1), E_evo(k), M_evo(k) )
        end do

    end if
  end subroutine iterator_obc


  subroutine metropolis_obc(E, M, new_E, new_M)
    !  one Monte Carlo step per spin
    integer :: ispin,x,y,dE
    integer, intent(in) :: E, M
    integer, intent(out) :: new_E, new_M
    real :: rnd

    new_E = E
    new_M = M

    do ispin = 1,N
      !     random x and y coordinates for trial spin
      call random_number(rnd)
      x = int(L*rnd) + 1
      call random_number(rnd)
      y = int(L*rnd) + 1
      dE = DeltaE_obc(x,y)
      call random_number(rnd)
      if (rnd <= w(dE)) then
        spin(x,y) = -spin(x,y)

        accept = accept + 1
        new_M = new_M + 2*spin(x,y)  ! factor 2 is to account for the variation:
        new_E = new_E + dE           ! (-(-)+(+))
        
      end if
    end do
  end subroutine metropolis_obc

  function DeltaE_obc(x,y) result (DeltaE_result)
    !  periodic boundary conditions
    integer, intent (in) :: x,y
    integer :: DeltaE_result
    integer :: left
    integer :: right
    integer :: up
    integer :: down
    if (x == 1) then
       left = 0
       right = spin(2,y)
    else if (x == L) then
       left = spin(L-1,y)
       right = 0
    else
       left = spin(x-1,y)
       right = spin(x+1,y)
    end if
    if (y == 1) then
       up = spin(x,2)
       down = 0
    else if (y == L) then
       up = 0
       down = spin(x,L-1)
    else
       up = spin(x,y+1)
       down = spin(x,y-1)
    end if
    DeltaE_result = 2*spin(x,y)*(left + right + up + down)
! also here the factor 2 is to account for the variation
  end function DeltaE_obc

end module isingf
