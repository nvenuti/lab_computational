import numpy as np
import math as m
import matplotlib.pyplot as plt
import sys
import os
import time
import cProfile
from pathlib import Path

from isingf import isingf

import funcs

#------------------------------------------------------------------------------
# handles a single evolution of the system can be executed on its own or called
# by other scripts that need to compute quantities over multiple evolutions

# execution of main() is governed by dictionary of key word aguments
# each added ad hoc to control a needed behaviour
#------------------------------------------------------------------------------


def main(**kwargs):

    if not os.path.isdir('./outputs'):
        os.mkdir('./outputs')

    if kwargs['show_time']:
        start = time.time()

    # kwargs that are typed many times
    gridsize = kwargs['gridsize']
    N_eq = kwargs['N_eq']
    N_mc = kwargs['N_mc']
    T = kwargs['T']
    B = kwargs['B']
    is_module = kwargs['is_module']
    use_saved_state = kwargs['use_saved_state']
    use_case = dict(no = [1, 2], impurity=[3, 4], radius=[5, 6])

    if kwargs['special_version'] == 'no':
        metro = isingf.metropolis
    elif kwargs['special_version'] == 'impurity':
        metro = isingf.metropolis_imp
    elif kwargs['special_version'] == 'radius':
        isingf.radius = kwargs['radius']
        isingf.radius2 = kwargs['radius']**2
        metro = isingf.metropolis_radius


    # pass specs for allocation
    isingf.l = gridsize
    isingf.n = gridsize**2
    isingf.nmcs   = N_mc
    isingf.nequil = N_eq
    # we want to handle allocation of the fortran arrays at the highest level
    # possible, so if we are calling main from something else, we are going to
    # do it there
    if not is_module:
        isingf.alloc()

    # for when we need 10 standard seeds
    # generated with https://www.random.org/sequences/
    seed_catalogue = [np.array([274, 604, 238, 538, 771, 364, 365, 632]),
                    np.array([456, 735, 479, 582, 903, 494, 723, 899]),
                    np.array([429, 404, 772, 123, 991, 678, 958, 511]),
                    np.array([874, 235, 896, 619, 819, 747, 122, 551]),
                    np.array([12, 876, 794, 954, 260, 708, 896, 703]),
                    np.array([528,  89, 139, 624,  42, 968, 957, 859]),
                    np.array([254, 994, 439, 457, 454, 568, 607, 952]),
                    np.array([485, 986, 925, 373, 676, 689, 162, 411]),
                    np.array([861, 606, 440, 167, 562, 891, 972, 808]),
                    np.array([306, 428, 837, 686, 270, 779, 214, 602])]

    if kwargs['set_seed']:
        if kwargs['set_seed'] == 'default':
            isingf.use_seed = np.array([31, 41, 26, 59, 53, 58, 97, 93], order='F'),
        else:
            isingf.use_seed = seed_catalogue[kwargs['set_seed']-1]

        isingf.set_seed()


    isingf.t = T
    isingf.b = B
    B_str = '{:03.0f}'.format(abs(B*100))
    isingf.init()

    # used by full_compute.py
    if use_saved_state:
        saved_step = kwargs['start_state']
        funcs.load_saved_state(B_str, saved_step, N_eq)
    else:
        funcs.make_initial(kwargs['initial_choice'], kwargs['special_version'])


    eq_data = np.zeros((N_eq,2))
    ev_data = np.zeros((N_mc,2))
    isingf.bond_count = np.zeros(N_mc, dtype=np.int32, order='F')
    isingf.bond_cm = np.zeros((N_mc, 2), dtype=np.int32,order='F')


    # if not is_module:
    #     funcs.show_pars(kwargs)


    # EQUILIBRATION
    # if we use a saved state we skip equilibration
    if not use_saved_state and kwargs['initial_choice'] != 'reuse_array':
        if not kwargs['stopmotion_eq']:
            # "fast" version
            #---------------------------------------------------------------------
            isingf.iterator( use_case[kwargs['special_version']][0] )
            eq_data[:, 0] = isingf.e_eq
            eq_data[:, 1] = isingf.m_eq
            #---------------------------------------------------------------------

        # otherwise equilibration is going to go step by step and we can insert
        # everything we might want in the middle
        else:
            eq_data[0, 0] = isingf.e_ini
            eq_data[0, 1] = isingf.m_ini

            if kwargs['visualization']:
                fig, ax = plt.subplots(layout="constrained")#, figsize=(10, 10))
                ax.set_xlim(-0.5, gridsize-0.5)
                ax.set_ylim(-0.5, gridsize-0.5)
                fig.suptitle('Spins')
                background = fig.canvas.copy_from_bbox(ax.bbox)
                
                pl = ax.imshow(isingf.spin, vmin=-1, vmax=1)
                fig.show()
                input('Press enter to move simulation forward')

            for i in range(1, N_eq):
                eq_data[i, 0], eq_data[i, 1] = metro(eq_data[i-1, 0], eq_data[i-1, 1])
                
                if kwargs['visualization']:
                    fig.canvas.restore_region(background)
                    # fig.suptitle(str(i))
                    pl.set_data(isingf.spin)
                    ax.draw_artist(pl)
                    fig.canvas.blit(ax.bbox)
                    # fig.canvas.draw()
                    if i%(1.0*N_eq/100) == 0:
                        print(i, end="\r", flush=True)
                        print()
                    input("\033[F")

        print('Eq. done')

        # used by test_eq.py
        if kwargs['only_eq']:
            eq_data = eq_data/isingf.n
            try:
                t_eq = np.where(np.abs(eq_data[:, 1]) > .99)[0][0]
            except:
                t_eq = 0
            return t_eq


        funcs.invert_b_field()


    # EVOLUTION
    # "fast" version
    #--------------------------------------------------------------------------
    if not kwargs['stopmotion_evo']:
        isingf.iterator( use_case[kwargs['special_version']][1] )
        ev_data[:, 0] = isingf.e_evo[:N_mc]
        ev_data[:, 1] = isingf.m_evo[:N_mc]
    #--------------------------------------------------------------------------
        # used by t_ncl.py and full_compute.py
        if B != 0.:
            try:
                M_crossover = np.where(ev_data[:, 1] < 0.8)[0][0]
                E_crossover = np.where(ev_data[:, 0] < (ev_data[0, 0]-100))[0][0]
                print('M crosses 0.5 at step         :', M_crossover)
                print('E crosses threshold at step   :', E_crossover)
            except:
                print('More MC steps needed to identify nucleation')
                M_crossover = -1
                E_crossover = -1

    # otherwise evolution is going to go step by step and we can insert
    # everything we might want in the middle
    else:
        ev_data[0, 0] = isingf.e_eq[N_eq-1]
        ev_data[0, 1] = isingf.m_eq[N_eq-1]
        isingf.bond_count[0] = -np.sum(isingf.bond)+2*(isingf.n)

        # reach point at which we might want to do something
        for i in range(1, kwargs['stop_ev_at']):
            ev_data[i, 0], ev_data[i, 1] = metro(ev_data[i-1, 0], ev_data[i-1, 1])
            isingf.bond_count[i] = -np.sum(isingf.bond)+2*(isingf.n)
            funcs.compute_bond_cm(i)

        if kwargs['visualization']:
            fig, ax = plt.subplots(layout="constrained")
            ax.set_xlim(-0.5, gridsize-0.5)
            ax.set_ylim(-0.5, gridsize-0.5)
            fig.suptitle('Spins')
            background = fig.canvas.copy_from_bbox(ax.bbox)
            pl = ax.imshow(isingf.spin, vmin=-1, vmax=1)
            fig.show()

            fig1, ax1 = plt.subplots(layout="constrained")
            ax1.set_xlim(-0.5, 2*gridsize)
            ax1.set_ylim(-0.5, 2*gridsize)
            fig1.suptitle('Bonds')
            background1 = fig1.canvas.copy_from_bbox(ax1.bbox)
            pl1 = ax1.imshow(isingf.bond, vmin=-2, vmax=2, cmap='RdGy')
            fig1.show()

            input('Press enter to move simulation forward')

        # continue evolving doing that thing
        for i in range(kwargs['stop_ev_at'], N_mc):
            ev_data[i, 0], ev_data[i, 1] = metro(ev_data[i-1, 0], ev_data[i-1, 1])
            isingf.bond_count[i] = -np.sum(isingf.bond)+2*(isingf.n)
            funcs.compute_bond_cm(i)

            if kwargs['visualization']:
                fig.canvas.restore_region(background)
                pl.set_data(isingf.spin)
                ax.draw_artist(pl)
                fig.canvas.blit(ax.bbox)

                fig1.canvas.restore_region(background1)
                pl1.set_data(isingf.bond)
                ax1.draw_artist(pl1)
                fig1.canvas.blit(ax1.bbox)
                
                if i%(1.0*N_mc/100) == 0:
                    print(i, end="\r", flush=True)
                    print()
                action = input("\033[F")
                # do we want to custom-save things?
                # if action == 'save':
                #     print('Saved state at step ', i)
                #     np.savetxt('./outputs/saved_step.txt', [i], fmt='%i')
                #     np.savetxt('./outputs/ev_data_'+str(i)+'.txt', ev_data[:i]/isingf.n)
                #     np.savetxt('./outputs/spin_'+str(i)+'.txt', isingf.spin, fmt='%i')
                #     np.savetxt('./outputs/bond_'+str(i)+'.txt', isingf.bond, fmt='%i')

            # used by full_compute.py
            if i in kwargs['save_at_step']:
                print('Automatically saved state at step ', i)
                # np.savetxt('./outputs/ev_data_'+str(i)+'.txt', ev_data[:i]/isingf.n)
                np.savetxt('./outputs/'+B_str+'_spin_'+str(i)+'.txt', isingf.spin, fmt='%i')
                np.savetxt('./outputs/'+B_str+'_bond_'+str(i)+'.txt', isingf.bond, fmt='%i')


    if not use_saved_state:
        print('Ev. done')

    # used by critical_temp.py
    if kwargs['eval_crit_temp']:
        return ev_data

    # used by autocorr.py
    if kwargs['compute_autocorr']:
        ev_data[:, 1] = np.abs(ev_data[:, 1])
        return ev_data


    eq_data = eq_data/isingf.n
    ev_data = ev_data/isingf.n


    if kwargs['save_txt']:
        np.savetxt('./outputs/eq_data.txt', eq_data)
        np.savetxt('./outputs/ev_data.txt', ev_data)

    if kwargs['save_figs']:
        funcs.plot_single(eq_data, ev_data, 
                        isingf.spin, isingf.bond_count, isingf.bond_cm, 
                        e_corr, m_corr, compute_autocorr=kwargs['compute_autocorr'],
                        T=isingf.t, B=isingf.b, stop_ev_at=kwargs['stop_ev_at'], N_mc=N_mc)

    if kwargs['show_time']:
        stop = time.time()
        print('main() took ', stop-start,' s')

    # used by full_compute.py
    if is_module:
        if not kwargs['save_at_step']:
            return E_crossover, M_crossover
        else:
            return ev_data[len(ev_data)-1, 0], ev_data[len(ev_data)-1, 1]

#------------------------------------------------------------------------------


if __name__ == "__main__":
    try:
        main(
        # if we need to genrate random numbers in python
        # rng             = np.random.default_rng(seed=42424),
        # side of the lattice
        gridsize        = 40,
        # intensity of magnetic field
        B               = 0.,
        # number of equilibration MC steps
        N_eq            = 10000,
        # number of evolution MC steps
        N_mc            = 2000,#-531755
        # temperature
        T               = 2.5,#2/m.log(1+2**0.5)*(4/9),
        # setting that controls what initial configuration is generated
        initial_choice  = 'random',
        # show duration of the single simulation
        show_time      = False,
        # procede step by step for the equilibration
        stopmotion_eq  = False,
        # procede step by step for the evolution, keep False to do all the evolution and find nucleation
        stopmotion_evo = False,
        # generate "video" of the system during execution
        visualization  = False,
        # MC time threshold used when working with stopmotion_evo, observe when nucleation is signalled and set this a bit earlier 
        stop_ev_at      = 1700,
        # MC step at which to save state of system to file
        save_at_step    = [0],#range(531768, 531790, 1),
        # save equilibration and evolution history of single execution
        save_txt        = True,
        # load a previously saved state controlled by save_at_step
        use_saved_state = False,
        # controls use of impurity or long range interaction
        special_version = 'no',
        # controls whether exam.py is highest level of execution
        is_module       = False,
        # choose which seed we want to use for fortran, =0 for random seed
        set_seed        = 'default',
        # save figures of single execution
        save_figs       = True,
        # stop after equilibration and return equilibration time
        only_eq         = False,
        # radius of long range interaction used if special_version == 'radius'
        radius          = 1,
        # just return evolution history to critical_temp.py
        eval_crit_temp  = False,
        # just return evolution history to autocorr.py
        compute_autocorr = False
            )
    except:
        import traceback, pdb, sys
        traceback.print_exc()
        pdb.post_mortem()
        sys.exit(1)



