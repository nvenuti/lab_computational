import numpy as np
import math as m
import matplotlib.pyplot as plt
import sys
import os
import time
import cProfile
from pathlib import Path

from isingf import isingf

from exam import main

#------------------------------------------------------------------------------
# compute autocorrelation for normalized e and m
def compute_autocorr(ev_data, corr_length):


    e_corr = np.zeros(corr_length)
    m_corr = np.zeros(corr_length)

    somma_iki = np.zeros((corr_length,2))
    somma_ik  = np.zeros((corr_length,2))
    somma_i   = np.zeros((corr_length,2))

    for k in range(corr_length):
        somma_iki[k, 0] = np.sum( ev_data[k:, 0] * ev_data[:isingf.nmcs-k, 0])
        somma_ik[k, 0]  = np.sum( ev_data[k:, 0])
        somma_i[k, 0]   = np.sum( ev_data[:isingf.nmcs-k, 0])

        somma_iki[k, 1] = np.sum( ev_data[k:, 1] * ev_data[:isingf.nmcs-k, 1])
        somma_ik[k, 1]  = np.sum( ev_data[k:, 1])
        somma_i[k, 1]   = np.sum( ev_data[:isingf.nmcs-k, 1])


    for k in range(corr_length):
        e_corr[k] = (somma_iki[k, 0] -(somma_ik[k, 0] *somma_i[k, 0]) /(isingf.nmcs - k) ) /(isingf.nmcs - k)
        m_corr[k] = (somma_iki[k, 1] -(somma_ik[k, 1] *somma_i[k, 1]) /(isingf.nmcs - k) ) /(isingf.nmcs - k)

    e_corr = e_corr / e_corr[0]
    m_corr = m_corr / m_corr[0]

    return e_corr, m_corr

#------------------------------------------------------------------------------

def make_results(**kwargs):

    # usual parameters
    #------------------------------------------------------------------------------
    gridsize = kwargs['gridsize']
    N_eq = kwargs['N_eq']
    N_mc = kwargs['N_mc']
    T = kwargs['T']

    isingf.l = gridsize
    isingf.n = gridsize**2
    isingf.nmcs   = N_mc
    isingf.nequil = N_eq
    #------------------------------------------------------------------------------


    # we allocate fortan arrays from the highest level of execution
    # every script we run is a single process, allocated varialbes will be freed
    # when process ends
    isingf.alloc()

    main_kwargs = dict(
            # rng             = np.random.default_rng(seed=42424),
            gridsize        = gridsize,
            B               = 0.,
            N_eq            = N_eq,
            N_mc            = N_mc,#-531755
            T               = T,#2/m.log(1+2**0.5)*(4/9),

            initial_choice  = 'random',

            show_time      = False,

            stopmotion_eq  = False,
            stopmotion_evo = False,
            visualization  = False,

            stop_eq_at      = 1,
            stop_ev_at      = 1700,
            save_at_step    = [0],#range(531768, 531790, 1),
            save_txt        = False,
            use_saved_state = False,
            special_version = 'no',
            is_module       = True,
            set_seed        = 0,#'default',
            save_figs       = False,
            only_eq         = False,

            radius          = 1,
            eval_crit_temp  = False,

            compute_autocorr = True
                )


    n_evos = 20

    corr_length = int(isingf.n/8)

    e_corr = np.zeros(corr_length)
    m_corr = np.zeros(corr_length)


    for i in range(n_evos):
        ev_data = main(**main_kwargs)

        e_array, m_array = compute_autocorr(ev_data, corr_length)

        e_corr = e_corr + e_array
        m_corr = m_corr + m_array


    e_corr = e_corr / n_evos
    m_corr = m_corr / n_evos


    # plot of autocorrelations
    fig, axs = plt.subplots(ncols=2, nrows=1,layout="constrained", figsize=(15, 10))
    fig.suptitle(r'Autocorrelation, T = '+str(isingf.t)+',  R = '+str(main_kwargs['radius']))
    
    axs[0].plot(range(len(e_corr)), e_corr, c='orange',label=r'E corr.')
    axs[0].set_yscale('log')

    axs[1].plot(range(len(m_corr)), m_corr, label=r'$|M|$ corr.')
    axs[1].set_yscale('log')

    for ax in axs:
        ax.legend()
        ax.grid()
        ax.set_xlabel('MC steps')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("./outputs/000_autocorr_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("./outputs/000_autocorr_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)


#------------------------------------------------------------------------------

if __name__ == "__main__":
    try:
        make_results(
        # rng             = np.random.default_rng(seed=42424),
        gridsize        = 20,
        N_eq            = 1000,
        N_mc            = 10000,#-531755
        bc              = 'pbc',
        T               = 4#2/m.log(1+2**0.5)*(4/9))
        )
    except:
        import traceback, pdb, sys
        traceback.print_exc()
        pdb.post_mortem()
        sys.exit(1)
