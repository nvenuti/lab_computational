import matplotlib.pyplot as plt
import numpy as np
import math as m
import sys
import time
from pathlib import Path
import json

# import multiprocessing

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18

from isingf import isingf
# from old_isingf import old_isingf as isingf


#------------------------------------------------------------------------------
# contains various funcitons that are needed in the evolution of the system
# done by exam.py and that could be useful in other scripts
#------------------------------------------------------------------------------

# makes necessary plots for single evolutions of the system done by exam.py
def plot_single(eq_data, ev_data, 
                spins, bond_count, bond_cm, 
                e_corr, m_corr,
                 **kwargs):

    T = kwargs['T']
    B = kwargs['B']

    B_str = '{:03.0f}'.format(abs(kwargs['B']*100))
    B_title = '{:02.0f}'.format(abs(kwargs['B']*100))


    # plot of equilibration history
    fig, axs = plt.subplots(ncols=2, nrows=1,layout="constrained", figsize=(15, 10))
    fig.suptitle(r'Equilibration, $T = 4/9 T_c$ , $|B|$ = 0.'+B_title)
    axs[0].plot(eq_data[:, 0], c='orange',label=r'$E/N$')
    axs[0].set_ylim([-2-abs(B)-0.01,0])

    axs[1].plot(eq_data[:, 1], label=r'$M/N$')
    
    axs[1].set_ylim([-1.01, 1.01])

    for ax in axs:
        ax.legend()
        ax.grid()
        ax.set_xlabel('MC steps')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("./outputs/"+B_str+"_single_eq_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("./outputs/"+B_str+"_single_eq_"+str(file_id)+".png")
    plt.savefig(file_path)

    # plot of evolution history
    fig, axs = plt.subplots(ncols=2, nrows=1,layout="constrained", figsize=(15, 10))
    fig.suptitle(r'Evolution, $T = 4/9 T_c$ , $|B|$ = 0.'+B_title)
    axs[0].plot(ev_data[:, 0], c='orange',label=r'$E/N$')
    axs[0].set_ylim([-2-abs(B)-0.01,0])

    axs[1].plot(ev_data[:, 1],label=r'$M/N$')
    axs[1].set_ylim([-1.01, 1.01])

    for ax in axs:
        ax.legend()
        ax.grid()
        ax.set_xlabel('MC steps')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("./outputs/"+B_str+"_single_ev_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("./outputs/"+B_str+"_single_ev_"+str(file_id)+".png")
    plt.savefig(file_path)


    # plot of nucleation cluster history
    if B != 0.:
        fig, axs = plt.subplots(ncols=3, nrows=1,layout="constrained", figsize=(15, 10))
        fig.suptitle(r'Nucleation, $T=4/9 T_c$ , $|B|$ = 0.'+B_title)
        
        axs[0].plot(range(kwargs['stop_ev_at'], kwargs['N_mc']), bond_cm[kwargs['stop_ev_at']:, 0], c='green',label=r'x pos')

        axs[1].plot(range(kwargs['stop_ev_at'], kwargs['N_mc']), bond_cm[kwargs['stop_ev_at']:, 1], c='blue', label=r'y pos')

        axs[2].plot(range(kwargs['stop_ev_at'], kwargs['N_mc']), bond_count[kwargs['stop_ev_at']:],c='red',label=r'bond count')
        # axs[2].set_yscale('log')

        for ax in axs:
            ax.legend()
            ax.grid()
            ax.set_xlabel('MC steps')

        # add new indexed file to directory
        file_id = 1
        file_path = Path("./outputs/"+B_str+"_single_ncl_"+str(file_id)+".png")
        while file_path.is_file():
            file_id += 1
            file_path = Path("./outputs/"+B_str+"_single_ncl_"+str(file_id)+".png")
        plt.savefig(file_path)


    # plot of final state of lattice
    fig, ax = plt.subplots(layout="constrained", figsize=(10, 10))
    fig.suptitle('Final state after '+str(kwargs['N_mc'])+' steps, T ='+str(T))

    ax.imshow(spins)

    file_id = 1
    file_path = Path("./outputs/"+B_str+"_snapshot_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("./outputs/"+B_str+"_snapshot_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)




# useful function recycled from the Python class for the Ising model
def get_up_right(i,j):
      
    if j==(isingf.l-1):
        right = 0
    else:
        right = j+1
    if i==(isingf.l-1):
        up = 0
    else:
        up = i+1

    return up,right


def calc_energy(L,spin, special_version=False):
    '''
    Calculates the total energy.
    NB: this is used only at initialization.
    '''

    if special_version == 'radius':
        E = 0
        for i in range(L):
            for j in range(L):
                near_spins = 0

                # try to exclude part of the grid when we can
                if (i-isingf.radius>=0 and i+isingf.radius<L):
                    xmax = i+isingf.radius+1
                    xmin = i-isingf.radius
                else:
                    xmax = L
                    xmin = 0
                  
                if (j-isingf.radius>=0 and j+isingf.radius<L):
                    ymax = j+isingf.radius+1
                    ymin = j-isingf.radius
                else:
                    ymax = L
                    ymin = 0

                for k in range(xmin, xmax):
                    for p in range(ymin, ymax):
                        if k == i and p == j:
                            continue

                        if distance(k, p, i, j, L) <= isingf.radius2:
                            # print(i, j, '|', k, p, '|', 'd =', distance(k, p, i, j, L))
                            near_spins += spin[k, p]

                E = E - spin[i,j]*(near_spins*2/isingf.neighbour_count[isingf.radius-1] + isingf.b)
    else:
        E = 0
        for i in range(L):
            for j in range(L):
                up, right = get_up_right(i, j)
                E = E - spin[i,j]*(spin[up, j]+spin[i,right]) -spin[i, j]*isingf.b

    return E

# compute squared distance between gridpoints with PBC
def distance(x2, y2, x1, y1, L):
    x_cath = x2-x1
    y_cath = y2-y1
    if (abs(x2-x1)>L/2):
        x_cath = abs(abs(x2-x1)-L)

    if (abs(y2-y1)>L/2):
        y_cath = abs(abs(y2-y1)-L)

    return x_cath*x_cath + y_cath*y_cath


# make initial grid how we want it
def make_initial(initial_setting, special_version):

    # array for visualizing bonds between negative spins
    isingf.bond = np.zeros((2*isingf.l, 2*isingf.l), dtype=np.int32,order='F')
    for i in range(isingf.l):
        for j in range(isingf.l):
            isingf.bond[2*i, 2*j] = 2

    if initial_setting == '1':
        # initialize grid with all +1
        isingf.spin = np.ones((isingf.l, isingf.l), dtype=np.int32,order='F') 

    elif initial_setting == 'random':
        # initialize grid with all +1
        isingf.spin = np.ones((isingf.l, isingf.l), dtype=np.int32,order='F') 
        # then generate random spins
        if special_version == 'no' or special_version == 'radius':
            isingf.initial_grid()
        elif special_version == 'impurity':
            isingf.initial_grid_imp()

    elif initial_setting == '5050':
        # initialize grid with all +1
        isingf.spin = np.ones((isingf.l, isingf.l), dtype=np.int32,order='F')
        # flip left half of grid
        isingf.spin[:, :int(isingf.l/2)] *= -1

    elif initial_setting == 'chess':
        # initialize grid with all +1
        isingf.spin = np.ones((isingf.l, isingf.l), dtype=np.int32,order='F')
        # make chessboard
        isingf.spin[::2, :] *= -1
        isingf.spin[:, ::2] *= -1

    elif initial_setting == 'reuse_array':
        # skip equilibration and reuse as initial state the current state of
        # the array from isingf
        isingf.e_eq[isingf.nequil-1] = calc_energy(isingf.l,isingf.spin, special_version)
        isingf.m_eq[isingf.nequil-1] = np.sum(isingf.spin)
        return 1

    else:
        print('Check \'initial_setting\' variable')
        sys.exit()


    # compute macroscopic properties with initial grid
    isingf.e_ini = calc_energy(isingf.l,isingf.spin, special_version)
    isingf.m_ini = np.sum(isingf.spin)


def invert_b_field():
    isingf.b = -isingf.b
    isingf.e_eq[isingf.nequil-1] -= 2*isingf.m_eq[isingf.nequil-1]*isingf.b
    isingf.init()


# used by full_compute.py
def load_saved_state(B_str, saved_step, N_eq):

    print('Loading saved state ', saved_step)

    isingf.spin = np.ones((isingf.l, isingf.l), dtype=np.int32,order='F')
    isingf.spin[:, :] = np.loadtxt('./outputs/'+B_str+'_spin_'+str(saved_step)+'.txt', dtype=np.int32)

    isingf.bond = np.zeros((2*isingf.l, 2*isingf.l), dtype=np.int32,order='F')
    isingf.bond[:, :] = np.loadtxt('./outputs/'+B_str+'_bond_'+str(saved_step)+'.txt', dtype=np.int32)

    # the evolution will read the macroscopic quantities of the starting state from here
    isingf.e_eq[N_eq-1] = calc_energy(isingf.l,isingf.spin)
    isingf.m_eq[N_eq-1] = np.sum(isingf.spin)

    invert_b_field()


def show_pars(kwdict):
    with open('pars.json', 'w') as fp:
        json.dump(kwdict, fp)



# computes geometrical position of the cluster of bonds
def compute_bond_cm(index):

    bonds_x = np.zeros(isingf.l)
    bonds_y = np.zeros(isingf.l)

    # compute weights of x positions and y positions
    for i in range(isingf.l):
        # if index == 4700:
        #     breakpoint()
        bonds_x[i] = np.sum(-isingf.bond[2*i+1, :])
        bonds_y[i] = np.sum(-isingf.bond[:, 2*i+1])

    # compute expectation values of x and y coord
    if np.sum(bonds_x) != 0:
        x_pos = np.sum( bonds_x* np.arange(1, isingf.l+1) ) / np.sum(bonds_x)
    else:
        x_pos = 0
    if np.sum(bonds_y) != 0:
        y_pos = np.sum( bonds_y* np.arange(1, isingf.l+1) ) / np.sum(bonds_y)
    else:
        y_pos = 0

    isingf.bond_cm[index, 0] = x_pos
    isingf.bond_cm[index, 1] = y_pos


#------------------------------------------------------------------------------
