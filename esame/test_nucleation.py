import matplotlib.pyplot as plt
import numpy as np
import math as m
import sys
import time
import cProfile
from sklearn.linear_model import LinearRegression
from pathlib import Path

# import multiprocessing

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18

from isingf import isingf
# from old_isingf import old_isingf as isingf

from funcs import *

from exam import main

#------------------------------------------------------------------------------

def test_nucleation():

    main_kwargs = dict(
                rng             = np.random.default_rng(seed=42424),
                gridsize        = 60,
                B               = 0.3,
                N_eq            = 100,
                N_mc            = 532180,#-531755
                bc              = 'pbc',
                T               = 2.269*(4/9),

                initial_choice  = 'random',

                stopmotion_eq  = False,
                stopmotion_evo = True,

                stop_eq_at      = 1,
                stop_ev_at      = 531741,
                save_at_step    = range(531768, 531790, 1),
                save_txt        = False,
                use_saved_state = True,
                special_version = 'no',
                is_module       = True,
                set_seed        = False
                        )

    start_points = np.loadtxt('./outputs/test_saved_step.txt').astype(np.int32)

    n_evos = 100

    final_state = np.zeros((len(start_points), n_evos, 2))
    res = np.zeros((len(start_points), 2))
    flip = np.zeros(len(start_points))


    isingf.l = gridsize
    isingf.n = gridsize**2
    isingf.nmcs   = N_mc
    isingf.nequil = N_eq

    isingf.alloc()


    for i in range(len(start_points)):
        for j in range(n_evos):
            final_state[i,j,0], final_state[i,j,1] = main( main_kwargs )

            if final_state[i, j, 1] < 0.5:
                flip[i] += 1

        res[i,0] = np.mean(final_state[i, :, 0]) 
        res[i,1] = np.mean(final_state[i, :, 1]) 

    np.savetxt('./outputs/test_final_state.txt', res)

    fig, axs = plt.subplots(ncols=3, nrows=1,layout="constrained", figsize=(15, 10))
    fig.suptitle('Final state, T ='+str(isingf.t))
    axs[0].plot(start_points, res[:, 0], c='orange',label=r'$E/N$')
    axs[0].set_ylim([-2-abs(B),0])

    axs[1].plot(start_points, res[:, 1], label=r'$M/N$')
    axs[1].set_ylim([-1, 1])

    axs[2].plot(start_points, flip/n_evos, color='red', label=r'% flips')


    for ax in axs:
        ax.legend()
        ax.grid()
        ax.set_xlabel('Start point')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("./outputs/nucleation_test_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("./outputs/nucleation_test_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)


#------------------------------------------------------------------------------

if __name__ == "__main__":
    try:
        test_nucleation()
    except:
        import traceback, pdb, sys
        traceback.print_exc()
        pdb.post_mortem()
        sys.exit(1)

