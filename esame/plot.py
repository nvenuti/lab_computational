import matplotlib.pyplot as plt
import numpy as np
import math as m


# testing bonds visualization 

gridsize = 10

rng = np.random.default_rng()#seed=42424)


grid = np.zeros((gridsize*2, gridsize*2))

for i in range(gridsize):
    for j in range(gridsize):
        grid[2*i, 2*j-1] = -1
        grid[2*i-1, 2*j] = -1

        if rng.random() < 0.3:
            grid[2*i, 2*j-1] = 1
        if rng.random() < 0.3:
            grid[2*i-1, 2*j] = 1

fig, ax = plt.subplots(layout="constrained")#, figsize=(10, 10))
ax.set_xlim(-0.5, 2*gridsize-0.5)
ax.set_ylim(-0.5, 2*gridsize-0.5)

# fig.suptitle('0')
pl = ax.imshow(grid, vmin=-1, vmax=1, cmap='bwr')

a = ax.get_xticks().tolist()
for i in range(len(a)):
    a[i] = str(float(a[i])/2)
ax.set_xticklabels(a)
ax.set_yticklabels(a)

fig.show()

input('Enter...')