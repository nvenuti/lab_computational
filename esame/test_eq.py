import matplotlib.pyplot as plt
import numpy as np
import math as m
import os
import sys
import time
# import cProfile
from pathlib import Path

# import multiprocessing

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18

from isingf import isingf
# from old_isingf import old_isingf as isingf

from exam import main


#------------------------------------------------------------------------------
# make histogram of the equilibration times obtained from many equilibrations
#------------------------------------------------------------------------------

def test_eq(**kwargs):

    if not os.path.isdir('./outputs'):
        os.mkdir('./outputs')

    # how many equilibration we want to compute
    n_evos = 200

    # usual parameters
    #------------------------------------------------------------------------------
    gridsize = kwargs['gridsize']
    N_eq = kwargs['N_eq']
    N_mc = kwargs['N_mc']
    T = kwargs['T']

    isingf.l = gridsize
    isingf.n = gridsize**2
    isingf.nmcs   = N_mc
    isingf.nequil = N_eq
    #------------------------------------------------------------------------------


    # we allocate fortan arrays from the highest level of execution
    # every script we run is a single process, allocated varialbes will be freed
    # when process ends
    isingf.alloc()


    main_kwargs = dict(
        # rng             = np.random.default_rng(seed=42424),
        gridsize        = kwargs['gridsize'],
        B               = 0.,
        N_eq            = kwargs['N_eq'],
        N_mc            = kwargs['N_mc'],#-531755
        bc              = 'pbc',
        T               = kwargs['T'],

        show_time      = False,

        initial_choice  = 'random',

        stopmotion_eq  = False,
        stopmotion_evo = False,
        visualization  = False,

        stop_eq_at      = 1,
        stop_ev_at      = 531741,
        save_at_step    = 0,#range(531768, 531790, 1),
        start_state     = 0,
        save_txt        = False,
        use_saved_state = False,
        special_version = 'no',
        is_module       = True,
        set_seed        = 0,#'default',
        save_figs       = False,
        only_eq         = True,

        radius          = 1,
        eval_crit_temp  = False,
        compute_autocorr = False

                        )

    t_eq = np.zeros(n_evos)
    for i in range(n_evos):
        t_eq[i] = main(**main_kwargs)

    # elements equal to zero mean we did not equilibrate
    # we exclude them as an easy way out
    # for large systems check how many runs we are loosing with 
    # len(t_eq)-len(t_eq_pruned)
    t_eq_pruned = t_eq[ t_eq != 0]

    np.savetxt('./outputs/000_test_eq.txt', t_eq)
    
    B_title = '{:02.0f}'.format(abs(main_kwargs['B']*100))


    # just plot the histogram
    fig, axs = plt.subplots(layout="constrained", figsize=(15, 10))
    # fig.suptitle(r'Equilibration time, $T = 4/9 T_c$ , R = '+str(main_kwargs['radius']))
    fig.suptitle(r'Equilibration time, $T = 4/9 T_c$ , $|B|$ = 0.'+B_title)
    axs.hist(t_eq_pruned, bins=40, range=(0, 200))

    # axs.legend()
    axs.grid()
    axs.set_xlabel('t_eq')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("./outputs/000_test_eq_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("./outputs/000_test_eq_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)



#------------------------------------------------------------------------------

if __name__ == "__main__":
    try:
        test_eq(
        # rng             = np.random.default_rng(seed=42424),
        gridsize        = 20,
        N_eq            = 200,
        N_mc            = 10,#-531755
        bc              = 'pbc',
        T               = 2/m.log(1+2**0.5)*(4/9)
                )
    except:
        import traceback, pdb, sys
        traceback.print_exc()
        pdb.post_mortem()
        sys.exit(1)

