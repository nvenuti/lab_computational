import matplotlib.pyplot as plt
import numpy as np
import math as m
import os
import sys
import time
# import cProfile
from pathlib import Path

# import multiprocessing

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18

from isingf import isingf
# from old_isingf import old_isingf as isingf

from exam import main

#------------------------------------------------------------------------------

def t_ncl(**kwargs):

    if not os.path.isdir('./outputs'):
        os.mkdir('./outputs')


    # usual parameters
    #------------------------------------------------------------------------------
    gridsize = kwargs['gridsize']
    N_eq = kwargs['N_eq']
    N_mc = kwargs['N_mc']
    T = kwargs['T']

    isingf.l = gridsize
    isingf.n = gridsize**2
    isingf.nmcs   = N_mc
    isingf.nequil = N_eq
    #------------------------------------------------------------------------------

    # we allocate fortan arrays from the highest level of execution
    # every script we run is a single process, allocated varialbes will be freed
    # when process ends
    isingf.alloc()
    t_ncl = np.zeros((len(kwargs['B_list']), 10))

    main_kwargs = dict(
        # rng             = np.random.default_rng(seed=42424),
        gridsize        = kwargs['gridsize'],
        B               = 0,
        N_eq            = kwargs['N_eq'],
        N_mc            = kwargs['N_mc'],#-531755
        bc              = 'pbc',
        T               = kwargs['T'],

        show_time      = False,

        initial_choice  = 'random',

        stopmotion_eq  = False,
        stopmotion_evo = False,
        visualization  = False,

        stop_eq_at      = 1,
        stop_ev_at      = 531741,
        save_at_step    = 0,#range(531768, 531790, 1),
        start_state     = 0,
        save_txt        = False,
        use_saved_state = False,
        special_version = 'impurity',
        is_module       = True,
        set_seed        = 0,
        save_figs       = False,
        only_eq         = False,
        eval_crit_temp  = False,
        compute_autocorr = False

                )

    for k in range(1,11):
        print('--------------------------------------------------------------------------')
        print('--------------------------------------------------------------------------')
        print(k)
        for i in range(len(kwargs['B_list'])):

            main_kwargs['set_seed'] = k
            main_kwargs['B'] = kwargs['B_list'][i]

            # perfected duration adjustment to not waste time doing useless evolution
            if main_kwargs['special_version'] == 'no':
                if kwargs['B_list'][i] < 0.36:
                    main_kwargs['N_mc'] = 400000
                elif kwargs['B_list'][i] < 0.41:
                    main_kwargs['N_mc'] = 70000
                elif kwargs['B_list'][i] < 0.46:
                    main_kwargs['N_mc'] = 12000
                elif kwargs['B_list'][i] < 0.51:
                    main_kwargs['N_mc'] = 7000
                elif 0.51 < kwargs['B_list'][i] < 0.71:
                    main_kwargs['N_mc'] = 1400
                elif kwargs['B_list'][i] > 0.71:
                    main_kwargs['N_mc'] = 200

            elif main_kwargs['special_version'] == 'impurity':
                if kwargs['B_list'][i] < 0.36:
                    main_kwargs['N_mc'] = 4000
                elif kwargs['B_list'][i] < 0.41:
                    main_kwargs['N_mc'] = 700
                elif kwargs['B_list'][i] < 0.46:
                    main_kwargs['N_mc'] = 500
                elif kwargs['B_list'][i] < 0.51:
                    main_kwargs['N_mc'] = 500
                elif 0.51 < kwargs['B_list'][i] < 0.71:
                    main_kwargs['N_mc'] = 200
                elif kwargs['B_list'][i] > 0.71:
                    main_kwargs['N_mc'] = 200

            print('--------------------------------------------------------------------------')
            print('B = ', kwargs['B_list'][i], ' N_mc = ', main_kwargs['N_mc'])

            B_str = '{:03.0f}'.format(abs(kwargs['B_list'][i]*100))

            # FARE EVOLUZIONE FINO A CHE NON TROVI INVERSIONE
            # print('Doing first evolution to find flip')
            e_inversion, t_ncl[i, k-1] = main(**main_kwargs)


    mean_t = np.zeros(len(kwargs['B_list']))

    for i in range(len(kwargs['B_list'])):
        B = kwargs['B_list'][i]
        column = t_ncl[i, :]
        adjust = column[ column > 0]
        print(f'For B = {B} excluded {len(column)-len(adjust)} values')
        mean_t[i] = np.mean(adjust)


    fig, axs = plt.subplots(layout="constrained", figsize=(15, 10))
    fig.suptitle('Nucleation time estimate')
    axs.plot(kwargs['B_list'], mean_t, c='red')

    axs.grid()
    axs.set_xlabel(r'$|B|$')
    axs.set_ylabel('MC steps')
    axs.set_yscale('log')


    # add new indexed file to directory
    file_id = 1
    file_path = Path("./outputs/t_ncl_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("./outputs/t_ncl_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)




#------------------------------------------------------------------------------

if __name__ == "__main__":
    try:
        t_ncl(
        B_list          = np.linspace(0.35, 0.95, 13),
        # rng             = np.random.default_rng(seed=42424),
        gridsize        = 60,
        N_eq            = 100,
        N_mc            = 400000,#-531755
        bc              = 'pbc',
        T               = 2/m.log(1+2**0.5)*(4/9)
                )
    except:
        import traceback, pdb, sys
        traceback.print_exc()
        pdb.post_mortem()
        sys.exit(1)

