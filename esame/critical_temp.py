import numpy as np
import math as m
import matplotlib.pyplot as plt
import sys
import os
import time
import cProfile
from pathlib import Path

# import multiprocessing

from isingf import isingf
# from old_isingf import old_isingf as isingf

import funcs

from exam import main

# scprit that computes the behaviour of the system around the critical temperature
# (possibly with interaction with radius)
#------------------------------------------------------------------------------

# first some functions only used here
#------------------------------------------------------------------------------

#plotting as a function of temperature
def plot_temps(T_list,res, gridsize, radius):
    # breakpoint()
    fig, axs = plt.subplots(ncols=2, nrows=2,layout="constrained", figsize=(15, 10))
    fig.suptitle('L = '+str(gridsize)+', R = '+str(radius))
    axs[0,0].errorbar(T_list,res[:,0], yerr=res[:,4],c='orange',label='energy', capsize=5, capthick=2)
    axs[0,1].errorbar(T_list,res[:,1], yerr=res[:,5],label='magnetisation', capsize=5, capthick=2)
    axs[1,0].errorbar(T_list,res[:,2], yerr=res[:,6],c='red',label='c (fluctuations)', capsize=5, capthick=2)
    axs[1,1].errorbar(T_list,res[:,3], yerr=res[:,7],c='black', label='magnetic susceptibility', capsize=5, capthick=2)
 

    # axs[0,0].plot(T_list,res[:,0], 'o-',c='orange',label='energy')
    # axs[0,1].plot(T_list,res[:,1], 'o-',label='magnetisation')
    # axs[1,0].plot(T_list,res[:,2], 'o-',c='red',label='c (fluctuations)')
    # axs[1,1].plot(T_list,res[:,3], 'o-',c='black',label='magnetic susceptibility')

    # numerical specific heat
    # midpoints = (T_list[:len(T_list)-1] + T_list[1:])/2
    # derivs = (res[1:, 0] - res[:len(res[:, 0])-1, 0])/(T_list[1:]-T_list[:len(T_list)-1])
    # axs[1,0].plot(midpoints, derivs,'x-',c='magenta',label='c (E derivative)')
    for axx in axs:
        for ax in axx:
            ax.grid()
            ax.legend()
            ax.set_xlabel('T')
            ax.set_xlim([T_list[0], T_list[len(T_list)-1]])
            ax.axvline(x=2.296,c='grey')

    # add new indexed file to directory
    file_id = 1
    file_path = Path("./outputs/multirun_"+str(file_id)+".png")
    while file_path.is_file():
        file_id += 1
        file_path = Path("./outputs/multirun_"+str(file_id)+".png")
    plt.savefig(file_path)

    print('file_id = ', file_id)

#------------------------------------------------------------------------------
def get_stats(data,N,T, mute=True):
        '''
        Calculate, store and print 
        statistical quantities
        '''
        E_N_av = np.average(data[:,0])/N
        M_N_av = np.average(np.abs(data[:,1]))/N
        c_heat = ( np.average(data[:,0]**2)- (E_N_av*N)**2 )/(T**2*N)
        chi_mag = ( np.average(data[:,1]**2)- (M_N_av*N)**2 )/(T*N)
        #print("Total number of steps run = {}".format(counter/N))
        #print("Acceptance ratio = {}".format(ar))
        if mute:
            pass
        else:
            print("Temperature = {}".format(T))
            print("<E/N> = {}".format(E_N_av))
            print("<M/N> = {}".format(M_N_av))
            print("specific heat = {}".format(c_heat))
            print("magnetic susceptibility = {}".format(chi_mag))
            print(sigma2_e**0.5, sigma2_m**0.5)
        return E_N_av,M_N_av,c_heat,chi_mag #, (sigma2_e**0.5)/N, (sigma2_m**0.5)/N


#------------------------------------------------------------------------------
# main routine
#------------------------------------------------------------------------------
def critical_temp(**kwargs):

    if not os.path.isdir('./outputs'):
        os.mkdir('./outputs')


    # how many runs we want to average across
    n_evos = 10

    # usual parameters
    #------------------------------------------------------------------------------
    gridsize = kwargs['gridsize']
    N_eq = kwargs['N_eq']
    N_mc = kwargs['N_mc']
    T_list = kwargs['T_list']

    isingf.l = gridsize
    isingf.n = gridsize**2
    isingf.nmcs   = N_mc
    isingf.nequil = N_eq
    #------------------------------------------------------------------------------


    # we allocate fortan arrays from the highest level of execution
    # every script we run is a single process, allocated varialbes will be freed
    # when process ends
    isingf.alloc()

    main_kwargs = dict(
        # rng             = np.random.default_rng(seed=42424),
        gridsize        = kwargs['gridsize'],
        B               = 0.,
        N_eq            = kwargs['N_eq'],
        N_mc            = kwargs['N_mc'],#-531755
        bc              = 'pbc',
        T               = 0,

        show_time      = False,

        initial_choice  = 'random',

        stopmotion_eq  = False,
        stopmotion_evo = False,
        visualization  = False,


        stop_eq_at      = 1,
        stop_ev_at      = 531741,
        save_at_step    = 0,#range(531768, 531790, 1),
        start_state     = 0,
        save_txt        = False,
        use_saved_state = False,
        special_version = 'radius',
        is_module       = True,
        set_seed        = 0,#'default',
        save_figs       = False,
        only_eq         = False,

        radius          = 3,
        eval_crit_temp  = True,
        compute_autocorr = False

                        )

    res_array = np.zeros((n_evos, len(T_list), 4))
    res_f = np.zeros((len(T_list), 8))
    ev_data = np.zeros((N_mc,2))

    # just do the work
    for i in range(n_evos):
        print(i, '/', n_evos)
        # nb only use this with n_evos = 10
        main_kwargs['set_seed'] = i+1
        for k in range(len(T_list)):
            main_kwargs['T'] = T_list[k]

            if k == 1:
                # reuse final state of array at previous temperature
                # as initial state for new temperature
                main_kwargs['initial_choice'] = 'reuse_array'

            ev_data = main(**main_kwargs)

            e,m,heat,chi = get_stats(ev_data,isingf.n,isingf.t)

            res_array[i, k, 0] = e
            res_array[i, k, 1] = m
            res_array[i, k, 2] = heat
            res_array[i, k, 3] = chi


    # compute the avrages over multiple evolutions
    for i in range(len(T_list)):
        res_f[i, 0] = np.mean(res_array[:, i, 0])
        res_f[i, 1] = np.mean(res_array[:, i, 1])
        res_f[i, 2] = np.mean(res_array[:, i, 2])
        res_f[i, 3] = np.mean(res_array[:, i, 3])
        res_f[i, 4] = (np.mean(res_array[:, i, 0]**2) - res_f[i, 0]**2)**0.5
        res_f[i, 5] = (np.mean(res_array[:, i, 1]**2) - res_f[i, 1]**2)**0.5
        res_f[i, 6] = (np.mean(res_array[:, i, 2]**2) - res_f[i, 2]**2)**0.5
        res_f[i, 7] = (np.mean(res_array[:, i, 3]**2) - res_f[i, 3]**2)**0.5


    plot_temps(T_list,res_f, gridsize, main_kwargs['radius'])




#------------------------------------------------------------------------------

if __name__ == "__main__":
    try:
        critical_temp(
        # rng             = np.random.default_rng(seed=42424),
        T_list          = np.linspace(1.5, 3.5, 10),
        gridsize        = 200,
        N_eq            = 1000,
        N_mc            = 50,#-531755
        bc              = 'pbc',
                )
    except:
        import traceback, pdb, sys
        traceback.print_exc()
        pdb.post_mortem()
        sys.exit(1)
