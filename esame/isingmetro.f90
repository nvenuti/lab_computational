module isingf
  implicit none
  public :: init, initial_grid, alloc, set_seed
  public :: metropolis, DeltaE, iterator, update_bonds
  public :: metropolis_imp, initial_grid_imp
  public :: metropolis_radius, DeltaE_radius, distance

  integer, public :: L,N,nmcs,nequil
  real (kind = 8), public, dimension(-8:8, 2) :: w
  integer, public, dimension(:,:), allocatable :: spin
  integer, public, dimension(:,:), allocatable :: bond, bond_cm
  real, public, dimension(:), allocatable :: E_eq, E_evo
  integer, public, dimension(:), allocatable :: M_eq, M_evo, bond_count
  integer, public, dimension(8) :: use_seed
  integer, public, dimension(10) :: neighbour_count
  integer, public, dimension(-1:1) :: column
  real (kind = 8), public :: T, B
  real, public :: E_ini
  integer, public :: M_ini, radius, radius2
  integer, public :: accept
contains

  subroutine set_seed()
    integer :: i
    real :: rn

    call random_seed(put=use_seed)

    ! flushing the first random numbers should guard against bad seeds
    do i=1, 100
      call random_number(rn)
    end do

    print*, "Seed set"
    ! ! check behaviour of seed
    ! open(unit=1, file="data.txt")
    ! do i=1, 100
    !   call random_number(rn)
    !   write(1, *) rn
    ! end do
    ! close(1)
  end subroutine set_seed

  subroutine init()
    integer :: dE
       
    column(-1) = 1
    column(0) = 0
    column(1) = 2

    do dE = -8,8,1
      ! for when spin is -1
      w(dE,1) = exp(-(dE-2*B)/T)
      ! for when spin is 1
      w(dE,2) = exp(-(dE+2*B)/T)
    end do

    neighbour_count(1) = 4
    neighbour_count(2) = 12
    neighbour_count(3) = 28
    neighbour_count(4) = 48
    neighbour_count(5) = 80
    neighbour_count(6) = 112
    neighbour_count(7) = 148
    neighbour_count(8) = 196
    neighbour_count(9) = 252
    neighbour_count(10) = 316
  end subroutine init


  subroutine alloc()
    allocate(spin(L,L))
    allocate(bond(2*L, 2*L))
    allocate(E_evo(nmcs))
    allocate(M_evo(nmcs))
    allocate(bond_count(nmcs))
    allocate(bond_cm(nmcs, 2))
    allocate(E_eq(nequil))
    allocate(M_eq(nequil))
  end subroutine alloc

  ! make random initial grid
  subroutine initial_grid()
    integer :: x, y
    real :: rnd

    do x = 1, L
      do y = 1, L
        call random_number(rnd)
        if (rnd >= 0.5) then
          spin(x, y) = 1
        else
          spin(x, y) = -1
        endif
      enddo
    enddo
  end subroutine initial_grid

  ! make random initial grid with impurity of 5 spins
  subroutine initial_grid_imp()
    integer :: x, y
    real :: rnd

    do x = 1, L
      do y = 1, L
        call random_number(rnd)
        if (rnd >= 0.5) then
          spin(x, y) = 1
        else
          spin(x, y) = -1
        endif
      enddo
    enddo

    ! part of the grid that will remain fixed
    spin(31, 31) = -1
    spin(30, 31) = -1
    spin(32, 31) = -1
    spin(31, 30) = -1
    spin(31, 32) = -1

    bond(62, 61) = -1
    bond(60, 61) = -1
    bond(61, 62) = -1
    bond(61, 60) = -1

  end subroutine initial_grid_imp

  ! handles all the equilibration/evolution inside of fortran,
  ! then back in python we can do whatever we want with the results
  subroutine iterator(use_case)
    integer :: k
    integer, intent(in) :: use_case


    select case (use_case)
    
    case (1)
        E_eq(1) = E_ini
        M_eq(1) = M_ini
        do k = 2, nequil
          call metropolis( E_eq(k-1), M_eq(k-1), E_eq(k), M_eq(k) )
        end do

    case (2)
        E_evo(1) = E_eq(nequil)
        M_evo(1) = M_eq(nequil)
        do k = 2, nmcs
          call metropolis( E_evo(k-1), M_evo(k-1), E_evo(k), M_evo(k) )
        end do

    case (3)
        E_eq(1) = E_ini
        M_eq(1) = M_ini
        do k = 2, nequil
          call metropolis_imp( E_eq(k-1), M_eq(k-1), E_eq(k), M_eq(k) )
        end do

    case (4)
        E_evo(1) = E_eq(nequil)
        M_evo(1) = M_eq(nequil)
        do k = 2, nmcs
          call metropolis_imp( E_evo(k-1), M_evo(k-1), E_evo(k), M_evo(k) )
        end do

    case (5)
        E_eq(1) = E_ini
        M_eq(1) = M_ini
        do k = 2, nequil
          call metropolis_radius( E_eq(k-1), M_eq(k-1), E_eq(k), M_eq(k) )
        end do

    case (6)
        E_evo(1) = E_eq(nequil)
        M_evo(1) = M_eq(nequil)
        do k = 2, nmcs
          call metropolis_radius( E_evo(k-1), M_evo(k-1), E_evo(k), M_evo(k) )
        end do

    case default
      print*, "Bad use_case"

    end select
  end subroutine iterator


  subroutine metropolis(E, M, new_E, new_M)
    !  one Monte Carlo step per spin
    integer :: ispin,x,y, dE
    real, intent(in) :: E
    integer, intent(in) :: M
    real, intent(out) :: new_E
    integer, intent(out) :: new_M

    real :: rnd

    new_E = E
    new_M = M

    do ispin = 1,N
       !     random x and y coordinates for trial spin
       call random_number(rnd)
       x = int(L*rnd) + 1
       call random_number(rnd)
       y = int(L*rnd) + 1
       dE = DeltaE(x,y)
       call random_number(rnd)
       if ( rnd <= w( dE, column(spin(x, y)) ) ) then
          spin(x,y) = -spin(x,y)
          call update_bonds(x, y)
          accept = accept + 1
          new_M = new_M + 2*spin(x,y)  ! factor 2 is to account for the variation:
          new_E = new_E + dE - 2*spin(x, y)*B          ! (-(-)+(+))
       
       end if
    end do
  end subroutine metropolis

  function DeltaE(x,y) result (DeltaE_result)
    !  periodic boundary conditions
    integer, intent (in) :: x,y
    integer :: DeltaE_result
    integer :: left
    integer :: right
    integer :: up
    integer :: down
    if (x == 1) then
      down = spin(L,y)
      up = spin(2,y)
    else if (x == L) then
      down = spin(L-1,y)
      up = spin(1,y)
    else
      down = spin(x-1,y)
      up = spin(x+1,y)
    end if
    if (y == 1) then
      right = spin(x,2)
      left = spin(x,L)
    else if (y == L) then
      right = spin(x,1)
      left = spin(x,L-1)
    else
      right = spin(x,y+1)
      left = spin(x,y-1)
    end if
    DeltaE_result = 2*spin(x,y)*(left + right + up + down)
! also here the factor 2 is to account for the variation
  end function DeltaE


! if new spin is +1 then surely we have to delete bonds
! otherwise check if we can register them
  subroutine update_bonds(x, y)
    integer, intent (in) :: x,y
    integer :: up, right, left, down
    integer :: bond_up, bond_right, bond_left, bond_down


    bond_up = 2*x
    bond_down = 2*x-2
    bond_right = 2*y
    bond_left = 2*y-2

    if (x == 1) then
      bond_down = 2*L
      down = spin(L,y)
      up = spin(2,y)
    else if (x == L) then
      down = spin(L-1,y)
      up = spin(1,y)
    else
      down = spin(x-1,y)
      up = spin(x+1,y)
    end if
    if (y == 1) then
      bond_left = 2*L
      right = spin(x,2)
      left = spin(x,L)
    else if (y == L) then
      right = spin(x,1)
      left = spin(x,L-1)
    else
      right = spin(x,y+1)
      left = spin(x,y-1)
    end if

    if (spin(x, y) > 0) then
      ! bond(2*x-1, 2*y-1) = 2
      bond(bond_up,    2*y-1) = 0
      bond(bond_down,  2*y-1) = 0
      bond(2*x-1, bond_right) = 0
      bond(2*x-1,  bond_left) = 0
    else
      ! bond(2*x-1, 2*y-1) = 1
      if (up < 0) then
        bond(bond_up,    2*y-1) = -1
      endif
      if (down < 0) then
        bond(bond_down,  2*y-1) = -1
      endif
      if (right < 0) then
        bond(2*x-1, bond_right) = -1
      endif
      if (left < 0) then
        bond(2*x-1,  bond_left) = -1
      endif
    endif
  end subroutine update_bonds


! evolution of the system that keeps impurity of 5 spins unchanged
!-------------------------------------------------------------------------------
  subroutine metropolis_imp(E, M, new_E, new_M)
    !  one Monte Carlo step per spin
    integer :: ispin,x,y, dE
    real, intent(in) :: E
    integer, intent(in) :: M
    real, intent(out) :: new_E
    integer, intent(out) :: new_M

    real :: rnd

    new_E = E
    new_M = M

    do ispin = 1,N
       !     random x and y coordinates for trial spin
      call random_number(rnd)
      x = int(L*rnd) + 1
      call random_number(rnd)
      y = int(L*rnd) + 1

      ! metodo stuido ma almeno è qualcosa
      do while ( (x == 31 .and. y == 31) .or. &
                (x == 30 .and. y == 31) .or.  &
                (x == 32 .and. y == 31) .or.  &
                (x == 31 .and. y == 30) .or.  &
                (x == 31 .and. y == 32) &
                )
        call random_number(rnd)
        x = int(L*rnd) + 1
        call random_number(rnd)
        y = int(L*rnd) + 1
      end do

      dE = DeltaE(x,y)
      call random_number(rnd)
      if ( rnd <= w( dE, column(spin(x, y)) ) ) then
          spin(x,y) = -spin(x,y)
          call update_bonds(x, y)
          accept = accept + 1
          new_M = new_M + 2*spin(x,y)  ! factor 2 is to account for the variation:
          new_E = new_E + dE - 2*spin(x, y)*B          ! (-(-)+(+))
       
      end if
    end do
  end subroutine metropolis_imp



! evolution of the system when we have an interaction radius beyond nearest neighbours
!-------------------------------------------------------------------------------
  subroutine metropolis_radius(E, M, new_E, new_M)
    !  one Monte Carlo step per spin
    integer :: ispin,x,y
    real, intent(in) :: E
    integer, intent(in) :: M
    real, intent(out) :: new_E
    integer, intent(out) :: new_M

    real :: rnd
    real ::dE

    new_E = E
    new_M = M

    do ispin = 1,N
       !     random x and y coordinates for trial spin
       call random_number(rnd)
       x = int(L*rnd) + 1
       call random_number(rnd)
       y = int(L*rnd) + 1
       dE = DeltaE_radius(x,y)
       call random_number(rnd)
       if ( rnd <= exp(-(dE+2*spin(x,y)*B)/T) ) then
          spin(x,y) = -spin(x,y)
          ! call update_bonds(x, y)
          accept = accept + 1
          new_M = new_M + 2*spin(x,y)  ! factor 2 is to account for the variation:
          new_E = new_E + dE - 2*spin(x, y)*B          ! (-(-)+(+))
          ! print*, dE
       
       end if
    end do
  end subroutine metropolis_radius



  function DeltaE_radius(x,y) result (DeltaE_result)
    !  periodic boundary conditions
    integer, intent (in) :: x,y
    real :: DeltaE_result
    integer :: near_spins
    integer :: i, j, xmin, xmax, ymin, ymax

    near_spins = 0

    ! we try to exclude parts of the grid when we can
    if (x-radius>0 .and. x+radius<L+1) then
      xmax = x+radius
      xmin = x-radius
    else
      xmax = L
      xmin = 1
    end if 
      
    if (y-radius>0 .and. y+radius<L+1) then
      ymax = y+radius
      ymin = y-radius
    else
      ymax = L
      ymin = 1
    end if 

    ! simple cycle through all the needed area
    do i = xmin, xmax
      do j = ymin, ymax
        if (0 < distance(i, j, x, y) .and. distance(i, j, x, y) <= radius2) then
          near_spins = near_spins + spin(i, j)
        endif
      enddo
    enddo

    DeltaE_result = real(2*spin(x,y)*near_spins*4, 8)/neighbour_count(radius)
    ! also here the factor 2 that accounts for the variation
  end function DeltaE_radius


! compute squared distance between gridpoints with PBC
  function distance(x2, y2, x1, y1) result (dist)
    integer, intent(in) :: x2, y2, x1, y1
    integer :: dist, x_cath, y_cath

    x_cath = x2-x1
    y_cath = y2-y1
    if (abs(x2-x1)>L/2) then
      x_cath = abs(abs(x2-x1)-L)
    end if
    if (abs(y2-y1)>L/2) then
      y_cath = abs(abs(y2-y1)-L)
    end if

    dist = x_cath*x_cath + y_cath*y_cath

  end function distance
!-------------------------------------------------------------------------------

end module isingf
