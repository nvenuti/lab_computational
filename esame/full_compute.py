import matplotlib.pyplot as plt
import numpy as np
import math as m
import os
import sys
import time
# import cProfile
from pathlib import Path

# import multiprocessing

plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["text.usetex"] = True
plt.rc('axes', labelsize=18)
plt.rc('legend',fontsize=18)
plt.rcParams['figure.titlesize']=18
plt.rcParams['xtick.labelsize']=18
plt.rcParams['ytick.labelsize']=18

from isingf import isingf
# from old_isingf import old_isingf as isingf

from exam import main

#------------------------------------------------------------------------------
# script that handles execution to rigorously compute nucleation time
# calls main() many times 
#------------------------------------------------------------------------------
def full_compute(**kwargs):

    if not os.path.isdir('./outputs'):
        os.mkdir('./outputs')


    # how many times we want to test each time value
    n_evos = 10

    # usual parameters
    #------------------------------------------------------------------------------
    gridsize = kwargs['gridsize']
    N_eq = kwargs['N_eq']
    N_mc = kwargs['N_mc']
    T = kwargs['T']

    isingf.l = gridsize
    isingf.n = gridsize**2
    isingf.nmcs   = N_mc
    isingf.nequil = N_eq
    #------------------------------------------------------------------------------


    # we allocate fortan arrays from the highest level of execution
    # every script we run is a single process, allocated varialbes will be freed
    # when process ends
    isingf.alloc()

    for i in range(len(kwargs['B_list'])):

        print('--------------------------------------------------------------------------')
        print('B = ', kwargs['B_list'][i])

        main_kwargs = dict(
            # rng             = np.random.default_rng(seed=42424),
            gridsize        = kwargs['gridsize'],
            B               = kwargs['B_list'][i],
            N_eq            = kwargs['N_eq'],
            N_mc            = kwargs['N_mc'],#-531755
            bc              = 'pbc',
            T               = kwargs['T'],

            show_time      = False,

            initial_choice  = 'random',

            stopmotion_eq  = False,
            stopmotion_evo = False,
            visualization  = False,

            stop_eq_at      = 1,
            stop_ev_at      = 531741,
            save_at_step    = 0,#range(531768, 531790, 1),
            start_state     = 0,
            save_txt        = False,
            use_saved_state = False,
            special_version = 'no',
            is_module       = True,
            set_seed        = 'default',
            save_figs       = False,
            only_eq         = False,

            radius          = 3,
            eval_crit_temp  = False,
            compute_autocorr = False

                    )

        # we discover these vivendo
        if kwargs['B_list'][i] == 0.35:
            main_kwargs['N_mc'] = 230000
        elif kwargs['B_list'][i] == 0.4:
            main_kwargs['N_mc'] = 90000
        elif kwargs['B_list'][i] == 0.45:
            main_kwargs['N_mc'] = 23000

        # perfected duration adjustment for default seed
        # so we don't waste time doing useless evolution
        if main_kwargs['special_version'] == 'no':
            if kwargs['B_list'][i] < 0.36:
                main_kwargs['N_mc'] = 230000
            elif kwargs['B_list'][i] < 0.41:
                main_kwargs['N_mc'] = 90000
            elif kwargs['B_list'][i] < 0.46:
                main_kwargs['N_mc'] = 23000
            elif kwargs['B_list'][i] < 0.51:
                main_kwargs['N_mc'] = 10000
            elif 0.51 < kwargs['B_list'][i] < 0.71:
                main_kwargs['N_mc'] = 2000
            elif kwargs['B_list'][i] > 0.71:
                main_kwargs['N_mc'] = 500

        elif main_kwargs['special_version'] == 'impurity':
            if kwargs['B_list'][i] < 0.36:
                main_kwargs['N_mc'] = 4000
            elif kwargs['B_list'][i] < 0.41:
                main_kwargs['N_mc'] = 700
            elif kwargs['B_list'][i] < 0.46:
                main_kwargs['N_mc'] = 500
            elif kwargs['B_list'][i] < 0.51:
                main_kwargs['N_mc'] = 500
            elif 0.51 < kwargs['B_list'][i] < 0.71:
                main_kwargs['N_mc'] = 200
            elif kwargs['B_list'][i] > 0.71:
                main_kwargs['N_mc'] = 200


        B_str = '{:03.0f}'.format(abs(kwargs['B_list'][i]*100))
        B_title = '{:02.0f}'.format(abs(kwargs['B_list'][i]*100))

        # EVOLVE FOR LONG ENOUGH AND FIND WHEN NUCLEATION HAPPENED
        print('Doing first evolution to find flip')
        e_inversion, m_inversion = main(**main_kwargs)

        if e_inversion == -1:
            print('No E inversion found, using dummy value to save states')
            e_inversion = 300
        if m_inversion == -1:
            print('No M inversion found, using dummy value to save states')
            m_inversion = 300
            range_of_times = range(e_inversion-200, e_inversion-50, 100)

        # REGISTER INVERSION TIMES
        # these precise bounds are set ad hoc for B in range [0.35,0.45]
        else:
            range_of_times = range(e_inversion-90-10*(2-i), e_inversion-40-10*(2-i), 1)

        final_state = np.zeros((len(range_of_times), n_evos, 2))
        res = np.zeros((len(range_of_times), 2))
        flip = np.zeros(len(range_of_times))

        main_kwargs['stop_ev_at']       = range_of_times[0] -1
        main_kwargs['save_at_step']     = range_of_times
        main_kwargs['stopmotion_evo']   = True

        main_kwargs['save_figs']        = False
        # RIFARE EVOLUZIONE SALVANDO INTERVALLI ATTORNO A TEMPO DI INVERSIONE
        print('Doing second evolution to save states')
        main(**main_kwargs)

        # from now on we want to use random seeds
        main_kwargs['set_seed']         = False
        main_kwargs['use_saved_state']  = True
        main_kwargs['N_mc']             = 400
        main_kwargs['stop_ev_at']       = 1
        main_kwargs['save_at_step']     = [0]
        main_kwargs['save_figs']        = False



        for j in range(len(range_of_times)):
            for k in range(n_evos):
                
                main_kwargs['start_state'] = range_of_times[j]

                final_state[j, k,0], final_state[j, k,1] = main(**main_kwargs)

                if final_state[j, k, 1] < 0.5:
                    flip[j] += 1

            res[j,0] = np.mean(final_state[j, :, 0]) 
            res[j,1] = np.mean(final_state[j, :, 1]) 

        np.savetxt('./outputs/'+B_str+'_final_state.txt', res)


        fig, axs = plt.subplots(ncols=3, nrows=1,layout="constrained", figsize=(15, 10))
        fig.suptitle(r'$T = 4/9 T_c$ , $|B|$ = 0.'+B_title)

        axs[0].plot(range_of_times, res[:, 0], c='orange',label=r'$E/N$')
        axs[0].set_ylim([-2-abs(isingf.b),0])

        axs[1].plot(range_of_times, res[:, 1], label=r'$M/N$')
        axs[1].set_ylim([-1, 1])

        axs[2].plot(range_of_times, flip/n_evos, color='red', label=r'\% flips')


        for ax in axs:
            ax.legend()
            ax.grid()
            ax.set_xlabel('Start point')

        # add new indexed file to directory
        file_id = 1
        file_path = Path("./outputs/"+B_str+"_nucleation_test_"+str(file_id)+".png")
        while file_path.is_file():
            file_id += 1
            file_path = Path("./outputs/"+B_str+"_nucleation_test_"+str(file_id)+".png")
        plt.savefig(file_path)

        print('file_id = ', file_id)




#------------------------------------------------------------------------------

if __name__ == "__main__":
    try:
        full_compute(
        B_list          = np.linspace(0.35, 0.45, 3),
        # rng             = np.random.default_rng(seed=42424),
        gridsize        = 60,
        N_eq            = 100,
        N_mc            = 230000,#-531755
        bc              = 'pbc',
        T               = 2/m.log(1+2**0.5)*(4/9)
                )
    except:
        import traceback, pdb, sys
        traceback.print_exc()
        pdb.post_mortem()
        sys.exit(1)

